@extends('layouts.main')
@section('title', '| Billings')
@section('content')
@include('includes._modal_fees')
<style type="text/css">
    .text-success {
    color: lightgreen;
}
</style>

<div class="row">
    <div class="col-md-12">
        <div class="form-top">
            <div class="form-top-left">
                <h3>Billing History</h3>
                <p>Name: <i>{{ $subscriber->name }} {{ $subscriber->last_name }}</i></p>
                <p>Address: <i>{{ $subscriber->address }}</i></p>
                <p>Subscription Rate: <i>{{ $subscriber->subscription->rate }}</i></p>

                @if($subscriber->billings()->first())
                    @if($subscriber->deposit->amount != 0)
                    <p>Deposit: <i>{{ $subscriber->deposit->amount }}</i></p>
                    @endif
                {{-- <button class="btn btn-success" data-toggle="modal" data-target="#pay">Pay Balance</button> --}}
                <p><b>Lastest Due Date: <i>{{ Carbon\Carbon::parse($subscriber->billings->sortBy('billing_date')->last()->billing_date)->format('M d, Y')  }}</i></b></p>
                <p><b>Total Amount Due: <i>{{ $subscriber->billings->sum('balance') }} </i></b></p>
                @else
                <hr>
                <div class="alert alert-danger">
                    <div>This is an initial input user. Please see the index card of this user and input the totality of the balances.</div>
                </div>
                @endif
                <a href="{{route('collector.subscribers')}}" class="btn btn-lg btn-warning">GO BACK TO SUBSCRIBERS</a>
            </div>
            <div class="form-top-right">
                <i class="fa fa-history fa-spin"></i>
            </div>
        </div> {{-- form-top --}}
        <div class="form-bottom">
            <div class="table-responsive">
            <table class="table white-text">
                <thead>
                    <tr>
                        <th>Billing No.</th>
                        <th>Billing Date</th>
                        <th>Due Date</th>
                        <th>Amount Due</th>
                        <th>Balance</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $count = $subscriber->billings->count();
                        $billings = $subscriber->billings()->orderBy('id', 'desc')->get();
                    ?>
                    @forelse($billings as $billing)
                    <tr>
                        <td>{{ $count }}</td>
                        <td>{{ Carbon\Carbon::parse($billing->billing_date)->format('M d, Y') }}</td>
                        <td>{{ Carbon\Carbon::parse($billing->due_date)->format('M d, Y') }}</td>
                        <td>{{ $billing->amount_due }}</td>
                        <td>
                            @if($billing->balance == 0)
                                @if($billing->status == 'Paid')
                                0
                                <span class="text-success">(Date Paid: {{ @($billing->payments->last()->date_paid) }})</span>
                                @else
                                0
                                @endif
                            @else
                            {{ $billing->balance }}
                            @endif
                        </td>
                        <td>{{ $billing->status }}</td>
                        <td>
                            <button onclick="view_fees('{{$billing->id}}')" class="btn btn-info" data-toggle="modal" data-target="#fee">View</button>
                        </td>
                    </tr>
                    @include('includes._modal_editable_billing')
                    <?php $count--;?>
                    @empty
                    @endforelse
                </tbody>
            </table>
        </div> {{-- form-bottom --}}
        </div> {{-- form-bottom --}}
    </div> {{-- col-md-10 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script type="text/javascript">

    var billingFee = "{{ route('billing.fees') }}";

    function view_fees(id)
    {
        $.ajax({
        url: billingFee,
        type:"GET",
        data: {id:id},
        success: function(res) {
            // console.log(res);
            if(res.transfer_fee == null) {
                res.transfer_fee = 0;
            }
            if(res.installation_fee == null) {
                res.installation_fee = 0;
            }
            if(res.reconnection_fee == null) {
                res.reconnection_fee = 0;
            }
            if(res.subscription_fee == null) {
                res.subscription_fee = 0;
            }
            if(res.wire_fee == null) {
                res.wire_fee = 0;
            }
            if(res.wire_length == null) {
                res.wire_length = 0;
            }
            if(res.disconnection_fee == null) {
                res.disconnection_fee = 0;
            }
            if(res.deposit_fee == null) {
                res.deposit_fee = 0;
            }
            if(res.discount == null) {
                res.discount = 0;
            }
            $("#installation_fee").text(res.installation_fee);
            $("#disconnection_fee").text(res.disconnection_fee);
            $("#subscription_fee").text(res.subscription_fee);
            $("#transfer_fee").text(res.transfer_fee);
            $("#deposit_fee").text(res.deposit_fee);
            $("#reconnection_fee").text(res.reconnection_fee);
            $("#wire_fee").text(res.wire_fee);
            $("#discount").text(res.discount);
            $("#wire_length").text(res.wire_length + ' ft.');
            $("#total").text(res.subscription_fee + res.wire_fee + res.installation_fee + res.transfer_fee + res.reconnection_fee + res.deposit_fee + res.disconnection_fee - res.discount);
            }
        })
    }
</script>
@endsection
