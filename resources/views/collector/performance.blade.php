@extends('layouts.main')
@section('title', '| Collector Report')
@section('content')
<div class="row">
    <div class="col-md-8 col-sm-8 col-sm-offset-2 col-md-offset-2"">
        <button id="btn_print" class="btn btn-danger form-control">Print</button>
    </div> {{-- col-md-8 --}}
</div> {{-- row --}}
<br>
<div class="row">
    <div class="col-md-8 col-sm-8 col-sm-offset-2 col-md-offset-2"">
        <div class="form-top">
            <div class="form-daterange">
                <div class="form-group">
                    <form action="{{ route('collector-performance.payments') }}" method="get">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon search-button-custom">
                                <i class="fa fa-search" aria-hidden="true"></i>
                            </span>
                            <input readonly onchange="this.form.submit()" type="text" autocomplete="off" id="report_date" name="date" class="search-input-custom form-control">
                        </div>
                    </form>
                </div>
            </div>
        </div> {{-- form-top --}}
    </div> {{-- col-md-8 --}}
</div> {{-- row --}}
<br>
<div class="row" id="printTable">
    <div class="col-md-8 col-sm-8 col-sm-offset-2 col-md-offset-2">
        <div class="form-top">
            <div class="form-top-left">
                <h3>BroadCable Communications Inc.</h3>
                <h4>Performance</h4>
                <hr>
                <h4>Date : {{ Carbon\Carbon::parse($report)->format('F d, Y') }}</h4>
            </div>
            <div class="form-top-right">
                <i class="fa fa-user"></i>
            </div>
        </div> {{-- form-top --}}
        <div class="form-bottom">
            <div class="table-responsive">
                <table border="1" cellpadding="10" class="table white-text">
                    <thead>
                        <tr>
                            <th width="150">Name</th>
                            <th width="150">Official Receipt</th>
                            <th width="100">Date Paid</th>
                            <th width="100">Amount Paid</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($collections as $key => $value)
                        <tr>
                            <td>{{ @$value->subscriber()->last_name }}, {{ @$value->subscriber()->name }}</td>
                            <td>{{ $value->official_receipt }}</td>
                            <td>{{ $value->date_paid }}</td>
                            <td>{{ $value->amount_paid }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div> {{-- form-bottom --}}
    </div> {{-- col-md-8 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script type="text/javascript">

    function printData()
    {
       var divTable = document.getElementById("printTable");

       newWin= window.open("");
       newWin.document.write(divTable.outerHTML);
       newWin.print();
       newWin.close();
    }

    $('#btn_print').on('click',function(){
        printData();
    });

    $(function() {

        $("#report_date").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'M/dd/yy',
            yearRange: '1950:2050'
        });
    });


</script>
@endsection
