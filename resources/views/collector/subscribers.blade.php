@extends('layouts.main')
@section('title', '| Dashboard')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="form-top">
            <div class="form-top-left">
                <h3>Active Subscribers</h3>
                <p>Number of Subscriber: <i>{{ $count }}</i></p>
            </div>
            <div class="form-top-right">
                <i class="fa fa-user"></i>
            </div>
        </div> {{-- form-top --}}
        <div class="form-bottom">
            <form action="{{ route('collector.subscribers') }}" method="get">
                <div class="input-group input-group-lg">
                    <span class="input-group-addon search-button-custom">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </span>
                    <input type="text" autocomplete="off" name="search" placeholder="Search By Name or Address" class="search-input-custom form-control">
                    <input type="submit" class="hidden">
                </div>
            </form>
            <br>
            @if($subscribers->count() != 0)
            <div class="table-responsive">
                <table class="table white-text">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th width="200">Name</th>
                            <th width="100">Address</th>
                            <th width="100">Latest Due</th>
                            <th>Balance</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subscribers as $subscriber)
                        <tr>
                            <td>{{ $subscriber->id }}</td>
                            <td>{{ $subscriber->last_name }}, {{ $subscriber->name }}</td>
                            <td>{{ $subscriber->address }}</td>
                            @if($subscriber->billings()->first())
                            <td>{{ Carbon\Carbon::parse($subscriber->billings->sortBy('billing_date')->last()->billing_date)->format('M d, Y') }}</td>
                            @else
                            <td>No Billing</td>
                            @endif
                            <td>{{ $subscriber->billings->sum('balance') }}</td>

                            <td>
                                <button class="btn btn-success" data-toggle="modal" data-target="#pay-{{$subscriber->id}}">PAY BALANCE</button>
                                <a href="{{route('collector.ty', $subscriber->id)}}" class="btn btn-info">VIEW PROFILE</a>
                            </td>
                        </tr>



<div id="pay-{{$subscriber->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Payment</h4>
                <br>
                Name : <b>{{$subscriber->name}}</b>
                <br>
                Balance : <b>{{$subscriber->billings->sum('balance')}}</b>
            </div> {{-- modal-header --}}
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <form id="form-{{$subscriber->id}}" action="{{ route('collector.pay') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="subscriber_id" value="{{$subscriber->id}}">
                            <input type="hidden" name="billing_pending" value="Pending">
                            <input type="hidden" name="billing_paid" value="Paid">
                            <input type="hidden" name="billing_free" value="Free">
                            <div class="form-group">
                                <input onkeypress="return isNumberKey(event)" placeholder="Official Receipt.." type="number" name="official_receipt" class="form-control">
                            </div>
                            <div class="form-group">
                                <input required readonly="" type="text" value="{{auth()->user()->name}}" name="collector" class="form-control">
                            </div>

                            <div class="form-group">
                                <input required readonly placeholder="Date Paid.." type="text" autocomplete="off" name="date_paid" class="datepay form-control">
                            </div>
                            <div class="form-group">
                                <input required placeholder="Amount.." type="text" name="amount_paid" class="form-control">
                            </div>
                            <div class="form-group">
                                <input placeholder="Discount.." type="text" name="discount" class="form-control">
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                            <div class="form-group">
                                <input type="button" onclick="submitForm('{{$subscriber->id}}');" class="form-control btn btn-info submit-payment" value="Submit">
                            </div>
                                </div>
                                <div class="col-md-6">

                            <div class="form-group">
                                <button class="btn btn-primary form-control" data-dismiss="modal">Cancel</button>
                            </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div> {{-- modal-body --}}
        </div> {{-- modal-content --}}
    </div> {{-- modal-dialog --}}
</div> {{-- modal --}}

















                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="text-center">{{ $subscribers->appends(Request::only('search'))->render() }}</div>
            @else
            <div class="text-center">
                <h3 class="white-text">No result found..</h3>
            </div>
            @endif
        </div> {{-- form-bottom --}}
    </div> {{-- col-md-10 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script type="text/javascript">
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57))
        return false;

    return true;
}
</script>
<script>
        function submitForm(id) {
            swal({
                title: "Are you sure?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, submit it!",
                closeOnConfirm: false
            }, function(isConfirm){
                if (isConfirm) {
                    document.getElementById('form-'+id).submit();
                }
            });
        }
    $(function() {

        $(".datepay").keypress(function(event) {
            event.preventDefault();
        });

        $(".datepay").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'M/dd/yy',
            yearRange: '1950:2050'
        }).datepicker("setDate", new Date());



        // $('.submit-payment').on('click',function(e){
        //     e.preventDefault();
        //     var form = $(this).closest('form');

        // });

    });

</script>
@endsection
