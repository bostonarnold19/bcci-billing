@if(Session::has('flash_message'))
<div class="alert alert-success">{{ Session::get('flash_message') }}</div>
@endif
@if ($errors->any())
@foreach ($errors->all() as $error)
<div class="alert alert-danger">
	<div>{{ $error }}</div>
</div>
@endforeach
@endif
<div id="ajax-message"></div>
