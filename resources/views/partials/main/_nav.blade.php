<nav class="navbar navbar-inverse navbar-fixed-top nav-custom-bg-color">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li>
                <a class="navbar-brand nav-custom-text-color" href="#">BroadCable Communications Inc.</a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right hidden-xs">
            @if (Auth::user())

            @if(Auth::user()->hasRole('Collector'))
            <li>
                <a class="nav-custom-text-color" href="{{ route('collector-performance.payments') }}">Collector Performance</a>
            </li>
            <li>
                <a class="nav-custom-text-color" href="{{ route('export.subscribers.collector') }}">Export Subscriber</a>
            </li>
            @endif

            @if(Auth::user()->hasRole('Super Admin'))
            <li>
                <a class="nav-custom-text-color" href="{{ route('export.subscribers.fee') }}">Export Subscriber</a>
            </li>
            <li>
                <a class="nav-custom-text-color" href="{{ route('collector.daily-all.subscriber') }}">Collector Report</a>
            </li>
            <li>
                <a class="nav-custom-text-color" href="{{ route('collector.daily') }}">Report Daily</a>
            </li>
            <li>
                <a class="nav-custom-text-color" href="{{ route('collector.monthly') }}">Report Monthly</a>
            </li>
            @elseif(Auth::user()->hasRole('Accountant'))
            <li>
                <a class="nav-custom-text-color" href="{{ route('collector.daily-all.subscriber') }}">Collector Report</a>
            </li>

            <li>
                <a class="nav-custom-text-color" href="{{ route('collector.daily') }}">Report Daily</a>
            </li>
            <li>
                <a class="nav-custom-text-color" href="{{ route('collector.monthly') }}">Report Monthly</a>
            </li>
            @endif

            @if(Auth::user()->hasRole('Accountant') && Auth::user()->hasRole('Collector'))
            <li>
                <a class="nav-custom-text-color" href="{{ route('dashboard') }}">Subscribers</a>
            </li>
            @elseif(Auth::user()->hasRole('Accountant'))
            <li>
                <a class="nav-custom-text-color" href="{{ route('dashboard') }}">Subscribers</a>
            </li>
            @elseif(Auth::user()->hasRole('Super Admin'))
            <li>
                <a class="nav-custom-text-color" href="{{ route('dashboard') }}">Subscribers</a>
            </li>
            <li>
                <a class="nav-custom-text-color" href="{{ route('activity.index') }}">Activity Logs</a>
            </li>
            <li>
                <a class="nav-custom-text-color" href="{{ route('get.reports') }}">Reports</a>
            </li>
            @elseif(Auth::user()->hasRole('Collector'))
            <li>
                <a class="nav-custom-text-color" href="{{ route('collector.subscribers') }}">Subscribers</a>
            </li>
            @endif
            <li class="dropdown">
                <a href="#" class="dropdown-toggle dropdown-toggle-custom" data-toggle="dropdown" role="button" aria-expanded="true">
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-custom" role="menu">
                    @role('Super Admin')
                    <li><a class="dropdown-list-custom" href="{{ route('get.collectors') }}">Collectors</a></li>
                    @endrole

                    @role('Accountant')
                    <li><a class="dropdown-list-custom" href="{{ route('get.custom') }}">Custom Data Input</a></li>
                    @endrole
                    <li class="divider"></li>
                    <li><a class="dropdown-list-custom" href="{{ route('logout') }}">Logout</a></li>
                </ul>
            </li>
            @endif
        </ul>
    </div> {{-- container --}}
</nav> {{-- nav --}}
