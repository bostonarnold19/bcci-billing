<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Broad Cable Communications Inc. @yield('title')</title>
<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="{{url('https://fonts.googleapis.com/css?family=Roboto:400,100,300,500')}}">
<link rel="stylesheet" href="{{url('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{url('/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('/css/jquery-ui.min.css')}}">
<!-- Custom CSS -->
<link rel="stylesheet" href="{{url('/css/custom-inputs.css')}}">
<link rel="stylesheet" href="{{url('/css/custom-form.css')}}">
<link rel="stylesheet" href="{{url('/css/custom-nav.css')}}">
<link rel="stylesheet" href="{{url('/css/sweetalert.css')}}">
<link rel="stylesheet" href="{{url('/css/custom-contents.css')}}">


