@extends('layouts.main')
@section('title', '| Login')
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="form-top">
            <div class="form-top-left">
                <h3>Login</h3>
                <p>Enter your email and password to log on:</p>
            </div>
            <div class="form-top-right">
                <i class="fa fa-lock"></i>
            </div>
        </div> {{-- form-top --}}
        <div class="form-bottom">
            <form role="form" action="{{ route('login.post') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <input type="text" required name="email" value="{{ old('email') }}" placeholder="Email Address.." class="bci-custom-input form-control">
                </div>
                <div class="form-group">
                    <input required type="password" name="password" placeholder="Password.." class="bci-custom-input form-control">
                </div>
                <button type="submit" class="bci-custom-button">Login!</button>
            </form>
        </div> {{-- form-bottom --}}
    </div> {{-- col-md-8 --}}
</div> {{-- row --}}
@endsection