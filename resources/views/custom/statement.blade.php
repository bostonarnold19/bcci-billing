<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <style type="text/css">
    @media print {
      #printableArea {
        -webkit-print-color-adjust: exact;
      }
    }
    @media print {
    .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
    float: left;
    }
    .col-sm-12 {
    width: 100%;
    }
    .col-sm-11 {
    width: 91.66666667%;
    }
    .col-sm-10 {
    width: 83.33333333%;
    }
    .col-sm-9 {
    width: 75%;
    }
    .col-sm-8 {
    width: 66.66666667%;
    }
    .col-sm-7 {
    width: 58.33333333%;
    }
    .col-sm-6 {
    width: 50%;
    }
    .col-sm-5 {
    width: 41.66666667%;
    }
    .col-sm-4 {
    width: 33.33333333%;
    }
    .col-sm-3 {
    width: 25%;
    }
    .col-sm-2 {
    width: 16.66666667%;
    }
    .col-sm-1 {
    width: 8.33333333%;
    }
.vertical-align {
    display: flex;
    align-items: center;
}
    }
    </style>
    <title>BCCI</title>
  </head>
  <body>
    <div class="container" id="printableArea">
      <div class="row">
        <div class="col-sm-8">
          <h4>BROADCABLE COMMUNICATIONS INC.</h4>
        </div>
        <div class="col-sm-4">
          <h4 class="text-right"><b>Statement</b></h4>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-7">
          <img src="http://broadcable.ph/wp-content/uploads/2017/05/150px-BroadCable-Logo.png" height="130">
        </div>
        <div class="col-sm-5">
          <div class="form-group">
            <div class="row vertical-align">
              <div class="col-sm-4">
                <label for="date">Date:</label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="date" placeholder="Date" value="{{ date('M d, Y') }}">
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row vertical-align">
              <div class="col-sm-4">
                <label for="statement"><small>Statement #:</small></label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="statement" placeholder="Statement #" value="S-{{ $subscriber->id }}">
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row vertical-align">
              <div class="col-sm-4">
                <label for="customer_id"><small>Customer ID:</small></label>
              </div>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="customer_id" placeholder="Customer ID" value="BCCI-{{ $subscriber->id }}">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-7">
          <div class="row">
            <div class="col-sm-5">
              <table class="table">
                <thead>
                  <tr>
                    <th style="background: #3C5282 !important; color: white !important;">Bill To:</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{{ $subscriber->last_name }}, {{ $subscriber->name }}<p style="width: 120px;">{{ $subscriber->address }}</p></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-sm-5">
          <table class="table">
            <thead>
              <tr>
                <th style="background: #3C5282 !important; color: white !important;">Account Summary:</th>
                <th style="background: #3C5282 !important; color: white !important;"></th>
                <th style="background: #3C5282 !important; color: white !important;"></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Previous Balance</td>
                <td>₱</td>
                <td>{{ number_format($billings->slice(0, -1)->sum('balance'),2)}}</td>
              </tr>
              <tr>
                <td>Credits</td>
                <td>₱</td>
                <td>0</td>
              </tr>
              <tr>
                <td>New Charges</td>
                <td>₱</td>
                <td>{{ number_format($billings->last()->balance,2)}}</td>
              </tr>
              <tr>
                <td><b>Total Balance Due</b></td>
                <td>₱</td>
                <td style="color: #856404 !important; background: #fff3cd !important; border-color: #ffeeba !important;">{{ number_format($billings->sum('balance'),2)}}</td>
              </tr>
              <tr>
                <td><b>Payment Due Date</b></td>
                <td></td>
                <td style="color: #721c24 !important; background: #f8d7da !important; border-color: #f5c6cb !important;">{{Carbon\Carbon::parse($billings->last()->due_date)->format('M d, Y')}}</td>
              </tr>
            </tbody>
          </table>
        </div>

      </div>


      <div class="row">
        <div class="col-sm-12">
        <table class="table table-bordered">
            <thead>
              <tr>
                <th style="background: #3C5282 !important; color: white !important;">Date</th>
                <th style="background: #3C5282 !important; color: white !important;">Invoice #</th>
                <th style="background: #3C5282 !important; color: white !important;">Description</th>
                <th style="background: #3C5282 !important; color: white !important;">Charges</th>
                <th style="background: #3C5282 !important; color: white !important;">Credits</th>
                <th style="background: #3C5282 !important; color: white !important;">Line Total</th>
              </tr>
            </thead>
            <tbody>
                @foreach($billings as $billing)
                <tr>
                  <td>{{ Carbon\Carbon::parse($billing->billing_date)->format('M d, Y') }}</td>
                  <td>BL-{{ $billing->id}}</td>
                  @if($loop->last)
                  <td>New Charge</td>
                  @else
                  <td>Remaining balance for the month of {{ Carbon\Carbon::parse($billing->billing_date)->format('F') }}</td>
                  @endif
                  <td>₱ {{ number_format($billing->balance, 2) }}</td>
                  <td></td>
                  <td>₱ {{ number_format($billing->balance, 2) }}</td>
                </tr>
                @endforeach
                <?php
$countBil = $billings->count();

?>

@if($countBil <= 20)
                @for ($i = 0; $i < 20-$countBil; $i++)
                <tr>
                  <td>-</td>
                  <td>-</td>
                  <td>-</td>
                  <td>-</td>
                  <td>-</td>
                  <td>-</td>
                </tr>
                @endfor
                @endif
            </tbody>
            <tfoot>
                <tr>
                <td style="background: #3C5282 !important; color: white !important;"></td>
                <td style="background: #3C5282 !important; color: white !important;"></td>
                <td style="background: #3C5282 !important; color: white !important;"></td>
                <td style="background: #3C5282 !important; color: white !important;"></td>
                <td style="background: #3C5282 !important; color: white !important;">Account Current Balance</td>
                <td style="background: #3C5282 !important; color: white !important;">₱ {{ number_format($billings->sum('balance'),2)}}</td>
              </tr>
            </tfoot>
          </table>
      </div>
      </div>




      <div class="row">
        <div class="col-sm-12">
<center>
          <p><small>Your account balance is ₱ {{ number_format($billings->sum('balance'),2)}} Please make your payment to cover the balance by the due date.</small></p>
            <p>Make all checks payable to BROADCABLE COMMUNICATIONS INC.</p>
            <p><b>Thank you for your business!</b></p>
            <p>Should you have any enquiries concerning this statement, please contact 09338103986 / 09175861966</p>
            <div style="border: 1px solid black !important;"></div>
            <p>912 MARBEA SUBDIVISION, MAGALANG, PAMPANGA, 2011</p>
            <p>Tel: 045-3434-580 Fax: 045-3434-580 E-mail: broadcablecommunications@yahoo.com</p>

          </center>
        </div>
      </div>


    </div>
    <!-- <button class="btn btn-default" onclick="printDiv('printableArea')">Print</button> -->
  </body>
  <script>
  function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
  }

  window.onload = function() { printDiv('printableArea'); }
  </script>
</html>
