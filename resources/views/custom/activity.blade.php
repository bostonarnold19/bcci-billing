@extends('layouts.main')
@section('title', '| Dashboard')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="form-top">
            <div class="form-top-left">
                <h3>Activity Logs</h3>
            </div>
            <div class="form-top-right">
                <i class="fa fa-user"></i>
            </div>
        </div> {{-- form-top --}}
        <div class="form-bottom">
            <form action="{{ route('activity.index') }}" method="get">
                <div class="input-group input-group-lg">
                    <span class="input-group-addon search-button-custom">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </span>
                    <input type="text" autocomplete="off" name="search" placeholder="Search" class="search-input-custom form-control">
                    <input type="submit" class="hidden">
                </div>
            </form>
            <br>
            @if($activities->count() != 0)
            <div class="table-responsive">
                <table class="table white-text">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>User</th>
                            <th>Description</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($activities as $activity)
                        <tr>
                            <td>{{ $activity->id }}</td>
                            <td>{{ $activity->user->name }}</td>
                            <td>{{ $activity->details }}</td>
                            <td>{{ $activity->created_at->timezone('Asia/Manila')->format('F d, Y g:i a') }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="text-center">{{ $activities->appends(Request::only('search'))->render() }}</div>
            @else
            <div class="text-center">
                <h3 class="white-text">No result found..</h3>
            </div>
            @endif
        </div> {{-- form-bottom --}}
    </div> {{-- col-md-10 --}}
</div> {{-- row --}}
@endsection
@section('scripts')

@endsection
