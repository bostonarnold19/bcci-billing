<center>
<a class="thumbnail" href="#" {{ $subscriber->picture != 'default.jpg' ? 'data-toggle=modal' : ''}} data-target="#picture">
	<img src="{{ asset('img/'.$subscriber->picture) }}" height="200" width="200" class="img-responsive" alt="picture">
</a>
@if(Request::is('dashboard/subscriber/profile/*'))
<div class="row">
	<button class="btn btn-info" onclick="document.getElementById('picture_upload').click();">
		<i class="fa fa-camera" aria-hidden="true"></i>
	</button>

	<button class="btn btn-danger" onclick="document.getElementById('form_delete').submit();">
		<i class="fa fa-times" aria-hidden="true"></i>
	</button>
	<form id="form_delete" role="form" action="{{ route('patch.subscriber.picture') }}" method="POST" files="true" enctype="multipart/form-data">
		<input type="file" name="picture" class="hide" value=""/>
		<input type="hidden" name="_method" value="PATCH">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="subscriber_id" value="{{ $subscriber->id }}">
		<div class="form-group">
	</form>
	<form role="form" action="{{ route('patch.subscriber.picture') }}" method="POST" files="true" enctype="multipart/form-data">
		<input type="file" id="picture_upload" name="picture" class="hide" onchange="this.form.submit()"/>
		<input type="hidden" name="_method" value="PATCH">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="subscriber_id" value="{{ $subscriber->id }}">
	</form>
</div>
@endif
</center>
<br>
<div class="text-center">
	<p><b id="subscriber_name_side_panel">{{ $subscriber->name }}</b></p>
</div>
<hr>
<div class="list-group">
	<a href="{{ route('billings.subscriber', $subscriber->id) }}" class="{{ Request::is('dashboard/subscriber/billings/*') ? "active" : "" }} list-group-item">Billing History</a>
	<a href="{{ route('payments.subscriber', $subscriber->id) }}" class="{{ Request::is('dashboard/subscriber/payments/*') ? "active" : "" }} list-group-item">Payments</a>
	<a href="{{ route('profile.subscriber', $subscriber->id) }}" class="{{ Request::is('dashboard/subscriber/profile/*') ? "active" : "" }} list-group-item">Subscriber Settings</a>
	<a href="{{ route('soa.subscriber', $subscriber->id) }}" class="{{ Request::is('dashboard/subscriber/statement-of-account/*') ? "active" : "" }} list-group-item">Statement of Account</a>
</div> {{-- list-group --}}