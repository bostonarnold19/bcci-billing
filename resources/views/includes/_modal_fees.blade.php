<div id="fee" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Summary</h4>
            </div> {{-- modal-header --}}
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-5">
                            <p>Subscription Fee:</p>
                            <p>Installation Fee:</p>
                            <p>Deposit Fee:</p>
                            <p>Transfer Fee:</p>
                            <p>Disconnection Fee:</p>
                            <p>Reconnection Fee:</p>
                            <p>Wire:</p>
                            <p>Wire Fee(1 ft. = ₱10):</p>
                            <hr>
                            <p>Discount:</p>
                            <hr>
                            <p>Total:</p>
                        </div>
                        <div class="col-md-5">
                            <p><i id="subscription_fee"></i>&nbsp;</p>
                            <p><i id="installation_fee"></i>&nbsp;</p>
                            <p><i id="deposit_fee"></i>&nbsp;</p>
                            <p><i id="transfer_fee"></i>&nbsp;</p>
                            <p><i id="disconnection_fee"></i>&nbsp;</p>
                            <p><i id="reconnection_fee"></i>&nbsp;</p>
                            <p><i id="wire_length"></i>&nbsp;</p>
                            <p><i id="wire_fee"></i>&nbsp;</p>
                            <hr>
                            <p><i id="discount"></i>&nbsp;</p>
                            <hr>
                            <p><b id="total"></b>&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div> {{-- modal-body --}}
            <div class="modal-footer">
            </div> {{-- modal-footer --}}
        </div> {{-- modal-content --}}
    </div> {{-- modal-dialog --}}
</div> {{-- modal --}}