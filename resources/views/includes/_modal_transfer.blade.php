<div id="transfer" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Transfer</h4>
            </div> {{-- modal-header --}}
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <h4>Transfer fee will be process?</h4>
                    </div>
                    <br>
                    <div class="row">
                        <form action="{{ route('post.transfer') }}" method="post">
                            <input type="hidden" name="id" value="{{ $subscriber->id }}">
                            <input type="hidden" name="wire_fee" value="10">
                            <input type="hidden" name="billing_status" value="Pending">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <input required type="text" id="billing_date" name="billing_date" class="readonly-bg-custom form-control" placeholder="Date of transfer.." autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="text" name="wire_length" class="form-control" placeholder="Additional Wire Length(ft)..">
                            </div>
                            <div class="form-group">
                                <input required type="text" name="transfer_fee" class="form-control" placeholder="Transfer Fee..">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info form-control">Confirm</button>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary form-control" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> {{-- modal-body --}}
            <div class="modal-footer">
            </div> {{-- modal-footer --}}
        </div> {{-- modal-content --}}
    </div> {{-- modal-dialog --}}
</div> {{-- modal --}}