<div id="reconnect" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reconnection</h4>
            </div> {{-- modal-header --}}
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <h4>Do you want to reconnect ?</h4>
                        <h3>{{ $subscriber->name }}</h3>
                    </div>
                    <br>
                    <div class="row">
                        <form action="{{ route('reconnect.subscriber') }}" method="post">
                            <input type="hidden" name="subscriber_id" value="{{ $subscriber->id }}">
                            {{-- <input type="hidden" name="amount_due" value="150"> --}}
                            <input type="hidden" name="_method" value="PATCH">
                            <input type="hidden" name="status" value="Pending">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Reconnection Fee" name="amount_due">
                            </div>
                            <div class="form-group">
                                <input required type="text" id="datereconnect" class="readonly-bg-custom form-control" name="reconnection_date" placeholder="Date of Reconnection.." autocomplete="off">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-info form-control" value="Confirm">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary form-control" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> {{-- modal-body --}}
            <div class="modal-footer">
            </div> {{-- modal-footer --}}
        </div> {{-- modal-content --}}
    </div> {{-- modal-dialog --}}
</div> {{-- modal --}}
