<div id="pay" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Payment</h4>
                <br>
                Name : <b>{{$subscriber->name}}</b>
                <br>
                Balance : <b>{{$subscriber->billings->sum('balance')}}</b>
            </div> {{-- modal-header --}}
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <form action="{{ route('post.pay') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="subscriber_id" value="{{$subscriber->id}}">
                            <input type="hidden" name="billing_pending" value="Pending">
                            <input type="hidden" name="billing_paid" value="Paid">
                            <input type="hidden" name="billing_free" value="Free">
                            <div class="form-group">
                                <input onkeypress="return isNumberKey(event)" placeholder="Official Receipt.." type="number" name="official_receipt" class="form-control">
                            </div>
                            <div class="form-group">
                                <select required class="form-control" name="collector" autocomplete="off">
                                    <option class="hide" disabled selected value="">Collector..</option>
                                    @foreach($collectors->users as $collector)
                                    <option value="{{ $collector->name }}">{{ $collector->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <input required placeholder="Date Paid.." type="text" id="datepay" autocomplete="off" name="date_paid" class="form-control">
                            </div>
                            <div class="form-group">
                                <input required placeholder="Amount.." type="text" name="amount_paid" class="form-control">
                            </div>
                            <div class="form-group">
                                <input placeholder="Discount.." type="text" name="discount" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="form-control btn btn-info" value="Submit">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary form-control" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> {{-- modal-body --}}
        </div> {{-- modal-content --}}
    </div> {{-- modal-dialog --}}
</div> {{-- modal --}}
<script type="text/javascript">
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57))
        return false;

    return true;
}
</script>
