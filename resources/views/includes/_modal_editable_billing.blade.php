<div id="billing-{{$billing->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editable Billing</h4>
            </div> {{-- modal-header --}}
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <form action="{{ route('billing.update', $billing->id) }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="PATCH">
                            <input type="hidden" name="subscriber_id" value="{{ $subscriber->id }}">
                            <p>Amount Due</p>
                            <div class="form-group">
                                <input value="{{ $billing->amount_due }}" placeholder="Amount Due.." type="text" name="amount_due" class="form-control">
                            </div>
                            <p>Balance</p>
                            <div class="form-group">
                                <input value="{{ $billing->balance }}" placeholder="Balance.." type="text" name="balance" class="form-control">
                            </div>
                                                                            <?php
$carbonFormat = Carbon\Carbon::createFromFormat('Y-m-d', $billing->billing_date);
$billing_date = Carbon\Carbon::parse($carbonFormat)->format('M/d/Y');

?>
                            <p>Billing Date</p>
                            <div class="form-group">
                                <input type="text" id="edit_billing_date" autocomplete="off" style="border: 1px solid #ccc;" required class="bci-custom-input form-control" name="billing_date" value="{{ $billing_date }}" placeholder="Billing date..">
                            </div>
                            <div class="form-group">
                                <input type="submit" class="form-control btn btn-info" value="Submit">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary form-control" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> {{-- modal-body --}}
        </div> {{-- modal-content --}}
    </div> {{-- modal-dialog --}}
</div> {{-- modal --}}
