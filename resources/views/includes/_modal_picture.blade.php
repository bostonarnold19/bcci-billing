<div id="picture" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <img src="{{ asset('img/'.$subscriber->picture) }}" height="500" width="500" class="thumbnail img-responsive" alt="picture">
    </div> {{-- modal-dialog --}}
</div> {{-- modal --}}