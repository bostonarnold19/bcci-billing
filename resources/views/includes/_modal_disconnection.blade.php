<div id="disconnect" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Disconnection</h4>
            </div> {{-- modal-header --}}
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <h4>Do you want to disconnect ?</h4>
                        <h3>{{ $subscriber->name }}</h3>
                    </div>
                    <br>
                    <div class="row">
                        <form action="{{ route('disconnect.subscriber') }}" method="post">
                            <input type="hidden" name="subscriber_id" value="{{ $subscriber->id }}">
                            <input type="hidden" name="_method" value="PATCH">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Disconnection Fee" name="amount_due">
                            </div>

                            <h4>Notes</h4>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="note" id="exampleRadios1" value="Not Paid" checked>
                              <label class="form-check-label" for="exampleRadios1">
                                Not Paid
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="note" id="exampleRadios2" value="End of Contract">
                              <label class="form-check-label" for="exampleRadios2">
                                End of Contract
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="note" id="exampleRadios3" value="Upgrade to Internet">
                              <label class="form-check-label" for="exampleRadios3">
                                Upgrade to Internet
                              </label>
                            </div>

                            <div class="form-group" style="margin-top: 20px;">
                                <input type="submit" class="btn btn-info form-control" value="Confirm">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary form-control" data-dismiss="modal">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div> {{-- modal-body --}}
            <div class="modal-footer">
            </div> {{-- modal-footer --}}
        </div> {{-- modal-content --}}
    </div> {{-- modal-dialog --}}
</div> {{-- modal --}}
