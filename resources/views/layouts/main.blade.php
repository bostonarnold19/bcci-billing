<!DOCTYPE html>
<html>
    <head>
        @include('partials.main._head')
        @yield('style')
    </head>
    <body>
        @include('partials.main._nav')
        <div class="container-fluid">
            @include('partials.main._messages')
            @yield('content')
        </div> {{-- container --}}
        @include('partials.main._footer')
        @include('partials.main._javascript')
        @yield('scripts')
    </body>
</html>