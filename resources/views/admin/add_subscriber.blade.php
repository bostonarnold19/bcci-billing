@extends('layouts.main')
@section('title', '| Add new subscriber')
@section('content')
<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <div class="form-top">
      <div class="form-top-left">
        <h3>Add New Subscriber</h3>
        <p>Fill in the form below:</p>
      </div>
      <div class="form-top-right">
        <i class="fa fa-pencil"></i>
      </div>
    </div> {{-- form-top --}}
    <div class="form-bottom">
      <form role="form" action="{{ route('store.subscriber') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="status" value="Active">
        <input type="hidden" name="wire_fee" value="10">
        <input type="hidden" name="billing_status" value="Pending">
        <div class="form-group">
          <input type="text" required class="bci-custom-input form-control" name="name" value="{{ old('name') }}" placeholder="First Name..">
        </div>
        <div class="form-group">
          <input type="text" required class="bci-custom-input form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name..">
        </div>
        <div class="form-group">
          <textarea required class="bci-custom-input form-control" name="address" placeholder="Address..">{{ old('address') }}</textarea>
        </div>
        <div class="form-group">
          <input type="text" class="bci-custom-input form-control" name="contact_number" value="{{ old('contact_number') }}" placeholder="Contact Number..">
        </div>
        <div class="form-group">
          <input required type="text" id="date_of_installation" class="readonly-bg-custom bci-custom-input form-control" name="date_of_installation" placeholder="Date of Installation.." autocomplete="off">
        </div>
        <div class="form-group">
          <input type="text" class="bci-custom-input form-control" name="wire_length" value="{{ old('wire_length') }}" placeholder="Addition Wire Length(ft)..">
        </div>
        <div class="form-group">
          <input type="text" required class="bci-custom-input form-control" name="subscriber_rate" value="{{ old('subscriber_rate') }}" placeholder="Subscription..">
        </div>
        <div class="form-group">
          <select required class="bci-custom-input form-control" name="deposit" value="{{ old('deposit') }}" placeholder="Deposit..">
              <option class="hide" disabled selected value="">Subscriber Type..</option>
              <option value="regular">Regular Subscriber</option>
              <option value="apartment">Apartment</option>
          </select>
        </div>
        <div class="form-group">
          <input type="text" required class="bci-custom-input form-control" name="installation_fee" value="{{ old('installation_fee') }}" placeholder="Installation Fee..">
        </div>
        <button type="submit" class="bci-custom-button">Save</button>
      </form>
    </div> {{-- form-bottom --}}
  </div> {{-- col-md-8 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script type="text/javascript">

  $(function() {

    $("#date_of_installation").keypress(function(event) {
      event.preventDefault();
    });

    $("#date_of_installation").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'M/dd/yy',
      yearRange: '1950:2050'
    });

  });
</script>
@endsection
