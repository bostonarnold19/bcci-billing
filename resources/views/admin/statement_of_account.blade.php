@extends('layouts.main')
@section('title', '| Billings')
@section('content')
@include('includes._modal_picture')
<div class="row">
	<div class="col-md-2 col-sm-2">
		@include('includes._side_panel')
	</div> {{-- col-md-2 --}}
	<div class="col-md-10 col-sm-10">
		<div class="form-top" id="printTable">
			<div class="form-daterange">
				<center><h3>BroadCable Communications Inc.</h3></center>
				<br>
				<center><h4 class="white-text">Statement of Account</h4></center>
				<hr>
				<br>
				<center><table border="1" cellpadding="10" class="table white-text">
					<tr>
						<td width="300"><p>Account Number</p></td>
						<td><p><b>{{ $subscriber->id }}</b></p></td>
					</tr>
					<tr>
						<td><p>Subscriber Name</p></td>
						<td><p><b>{{ $subscriber->name }} {{ $subscriber->last_name }}</b></p></td>
					</tr>
					<tr>
						<td><p>Billing Address</p></td>
						<td><p><b>{{ $subscriber->address }}</b></p></td>
					</tr>
					<tr>
						<td><p>Monthly Subscription Fee</p></td>
						<td><p><b>{{ $subscriber->subscription->rate }}</b></p></td>
					</tr>
					<tr>
						<td><p>Statement Period</p></td>
						<td><p><b>
						<?php
foreach ($billings->where('status', 'Pending') as $billing) {
    echo Carbon\Carbon::parse($billing->billing_date)->format('M d, Y') . ',';
}
?></b></p>
						</td>
					</tr>
					<tr>
						<td><p>Total Amount Due</p></td>
						<td><p><b>{{ $subscriber->billings->sum('balance') }}</b></p></td>
					</tr>
				</table></center>
			</div>
		</div> {{-- form-top --}}
		@if($subscriber->billings()->first())
		<a href="{{ route('statement.show', $subscriber->id) }}" target="_blank" class="btn btn-danger form-control">Print</a>
		@else
		<a href="{{ route('statement.show', $subscriber->id) }}" class="btn btn-danger form-control">Print</a>
		@endif
		{{-- <button id="btn_print" class="btn btn-danger form-control">Print</button> --}}
	</div> {{-- col-md-10 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
{{-- <script type="text/javascript">

    function printData()
	{
	   var divTable = document.getElementById("printTable");

	   newWin= window.open("");
	   newWin.document.write(divTable.outerHTML);
	   newWin.print();
	   newWin.close();
	}

	$('#btn_print').on('click',function(){
		printData();
	});

</script> --}}
@endsection
