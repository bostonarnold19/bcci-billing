@extends('layouts.main')
@section('title', '| Add old subscriber')
@section('content')
<div class="row">
  <div class="col-md-8 col-md-offset-2">
    <div class="form-top">
      <div class="form-top-left">
        <h3>Add Old Subscriber (Inactive/Disconnected Subscriber)</h3>
        <p>Fill in the form below:</p>
      </div>
      <div class="form-top-right">
        <i class="fa fa-pencil"></i>
      </div>
    </div> {{-- form-top --}}
    <div class="form-bottom">
      <form role="form" action="{{ route('post.add.old.subscriber') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="status" value="Inactive">
        <input type="hidden" name="billing_status" value="Pending">
        <div class="form-group">
          <input type="text" required class="bci-custom-input form-control" name="name" value="{{ old('name') }}" placeholder="First Name..">
        </div>
        <div class="form-group">
          <input type="text" required class="bci-custom-input form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last Name..">
        </div>
        <div class="form-group">
          <textarea required class="bci-custom-input form-control" name="address" placeholder="Address..">{{ old('address') }}</textarea>
        </div>
        <div class="form-group">
          <input type="text" class="bci-custom-input form-control" name="contact_number" value="{{ old('contact_number') }}" placeholder="Contact Number..">
        </div>
        <div class="form-group">
          <input type="text" required class="bci-custom-input form-control" name="subscriber_rate" value="{{ old('subscriber_rate') }}" placeholder="Subscription..">
        </div>
        <div class="form-group">
          <input type="text" required class="bci-custom-input form-control" name="balance" value="{{ old('balance') }}" placeholder="Total Balance..">
        </div>
        <button type="submit" class="bci-custom-button">Save</button>
      </form>
    </div> {{-- form-bottom --}}
  </div> {{-- col-md-8 --}}
</div> {{-- row --}}
@endsection
