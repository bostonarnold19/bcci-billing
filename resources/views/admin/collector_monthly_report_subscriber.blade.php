@extends('layouts.main')
@section('title', '| Collector Monthly Report')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12"">
        <button id="btn_print" class="btn btn-danger form-control">Print</button>
    </div> {{-- col-md-8 --}}
</div> {{-- row --}}
<br>
<div class="row" id="printTable">
    <div class="col-md-12 col-sm-12">
        <div class="form-top">
            <div class="form-top-left">
                <h3>BroadCable Communications Inc.</h3>
                <h4>Monthly Collections</h4>
                <hr>
                <h4>Date : {{ Carbon\Carbon::parse($start)->format('F Y') }}</h4>
                <h4>Collector : {{ $collector }}</h4>
            </div>
            <div class="form-top-right">
                <i class="fa fa-user"></i>
            </div>
        </div> {{-- form-top --}}
        <div class="form-bottom">
            <div class="table-responsive">
                <table border="1" cellpadding="10" class="table white-text">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Billing</th>
                            <th width="150">Official Receipt</th>
                            <th width="150">Date Paid</th>
                            <th width="150">Amount Paid</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($collections as $key => $value)
                        <tr>
                            <td>{{ @$value->subscriber()->last_name }}, {{ @$value->subscriber()->name }}</td>
                            <td>
                                {{ @$value->subscriber()->address }}
                            </td>
                            <td>
                                <?php
                                foreach (@$value->billings as $billing) {
                                echo Carbon\Carbon::parse(@$billing->billing_date)->format('M d, Y') . ',';
                                }
                                ?>
                            </td>
                            <td>{{ $value->official_receipt }}</td>
                            <td>{{ $value->date_paid }}</td>
                            <td>{{ $value->amount_paid }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div> {{-- form-bottom --}}
    </div> {{-- col-md-8 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script type="text/javascript">

    function printData()
    {
       var divTable = document.getElementById("printTable");

       newWin= window.open("");
       newWin.document.write(divTable.outerHTML);
       newWin.print();
       newWin.close();
    }

    $('#btn_print').on('click',function(){
        printData();
    });

</script>
@endsection
