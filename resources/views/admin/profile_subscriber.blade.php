@extends('layouts.main')
@section('title', '| Profile')
@section('content')
@include('includes._modal_transfer')
@include('includes._modal_picture')
<div class="row">
	<div class="col-md-2 col-sm-2">
		@include('includes._side_panel')
	</div> {{-- col-md-2 --}}
	<div class="col-md-10 col-sm-10">
		<div class="form-top">
			<div class="form-top-left">
				<h3>Subscriber Settings</h3>
				<hr>
				<button data-toggle="modal" data-target="#transfer" type="button" class="btn btn-danger">Generate Transfer Billing</button>
        		@role('Super Admin')
				<button type="submit" class="btn btn-danger" form="deleteSubscriber">Delete This Subscriber</button>
				<form id="deleteSubscriber" onsubmit="return confirm('Are you sure you want to delete this ?');" method="POST" action="{{ route('user-subscriber.destroy', $subscriber->id) }}">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
				</form>
				@endrole
			</div>
			<div class="form-top-right">
				<i class="fa fa-cog fa-spin"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<form role="form" action="{{ route('user-subscriber.update', $subscriber->id) }}" method="post">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="_method" value="PATCH">
                            <p>First Name</p>
				<div class="form-group">
					<input type="text" required class="bci-custom-input form-control" name="name" value="{{ $subscriber->name }}" placeholder="First Name..">
				</div>
				<p>Last Name</p>
				<div class="form-group">
					<input type="text" required class="bci-custom-input form-control" name="last_name" value="{{ $subscriber->last_name }}" placeholder="Last Name..">
				</div>
				<p>Address</p>
				<div class="form-group">
					<textarea required class="bci-custom-input form-control" name="address" placeholder="Address..">{{ $subscriber->address }}</textarea>
				</div>
				<p>Contact Number</p>
				<div class="form-group">
					<input type="text" class="bci-custom-input form-control" name="contact_number" value="{{ $subscriber->contact_number }}" placeholder="Contact Number..">
				</div>

				<p>Collector</p>
                <div class="form-group">
                    <select required class="bci-custom-input form-control" name="collector" autocomplete="off">
                        <option class="hide" disabled selected value="">Collector..</option>
                        @foreach($collectors->users as $collector)
                        <option <?php if($collector->name==$subscriber->collector) echo 'selected="selected"'; ?> value="{{ $collector->name }}">{{ $collector->name }}</option>
                        @endforeach
                    </select>
                </div>

				<p>Subscription Rate</p>
				<div class="form-group">
					<input type="text" autocomplete="off" required class="bci-custom-input form-control" name="rate" value="{{ $subscriber->subscription->rate }}" placeholder="Subscription..">
				</div>
				<div class="form-group">
					<?php
if (strtotime($subscriber->date_of_installation)) {
    $carbonFormat = Carbon\Carbon::createFromFormat('Y-m-d', $subscriber->date_of_installation);
    $date_of_installation = Carbon\Carbon::parse($carbonFormat)->format('M/d/Y');
} else {
    $date_of_installation = null;
}

?>
				<p>Date of Installation</p>

					<input type="text" id="date_of_installation" required class="bci-custom-input form-control" name="date_of_installation" value="{{ $date_of_installation }}" placeholder="Date of Installation">
				</div>
				<button type="submit" class="bci-custom-button">Save</button>
			</form>
		</div> {{-- form-bottom --}}
	</div> {{-- col-md-10 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script>
$(function() {
$("#date_of_installation").keypress(function(event) {
event.preventDefault();
});
$("#date_of_installation").datepicker({
changeMonth: true,
changeYear: true,
dateFormat: 'M/dd/yy',
yearRange: '1950:2050'
});
});
</script>
@endsection
