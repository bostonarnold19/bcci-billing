@extends('layouts.main')
@section('title', '| Collector Daily Report')
@section('content')
<div class="row">
	<div class="col-md-8 col-sm-8 col-sm-offset-2 col-md-offset-2"">
		<button id="btn_print" class="btn btn-danger form-control">Print</button>
	</div> {{-- col-md-8 --}}
</div> {{-- row --}}
<br>
<div class="row">
	<div class="col-md-8 col-sm-8 col-sm-offset-2 col-md-offset-2"">
		<div class="form-top">
			<div class="form-daterange">
				<div class="form-group">
					<form action="{{ route('collector.daily') }}" method="get">
						<div class="input-group input-group-lg">
							<span class="input-group-addon search-button-custom">
								<i class="fa fa-search" aria-hidden="true"></i>
							</span>
							<input readonly onchange="this.form.submit()" type="text" autocomplete="off" id="report_date" name="date" class="search-input-custom form-control">
						</div>
					</form>
				</div>
			</div>
		</div> {{-- form-top --}}
	</div> {{-- col-md-8 --}}
</div> {{-- row --}}
<br>
<div class="row" id="printTable">
	<div class="col-md-8 col-sm-8 col-sm-offset-2 col-md-offset-2">
		<div class="form-top">
			<div class="form-top-left">
				<h3>BroadCable Communications Inc.</h3>
				<h4>Daily Collections</h4>
				<a href="{{ route('collector.daily') }}" class="btn btn-warning">Collector's Daily Report</a>
				<a href="{{ route('collector.monthly') }}" class="btn btn-success">Collector's Monthly Report</a>
				<hr>
				<h4>Date : {{ Carbon\Carbon::parse($report)->format('F d, Y') }}</h4>
			</div>
			<div class="form-top-right">
				<i class="fa fa-user"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<div class="table-responsive">
				<table border="1" cellpadding="10" class="table white-text">
					<thead>
						<tr>
							<th>Name</th>
							<th class="text-right" width="150">Total Collection</th>
							<th class="text-right" width="150">Count Collection</th>
							<th class="text-right" width="150">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $temp = 0;?>
						@foreach($collectors as $key => $value)
						<tr>
							<td>{{$value->collector}}</td>
							<td class="text-right">
								{{ number_format($value->amount_paid,2) }}
								{{-- {{ number_format($value->where('date_paid', $report)->sum('amount_paid'),2) }} --}}
							</td>
							<td class="text-right">{{$value->count}}</td>
							<td>
								<button data-collector="{{$value->collector}}" class="btn btn-primary view-subscribers">View Subscribers</button>
							</td>
						</tr>
						<?php $temp = $value->amount_paid + $temp;?>
						@endforeach
						<tr>
							<td class="text-right"><b>Total :</b></td>
							<td class="text-right"><b>{{ number_format($temp,2) }}</b></td>
							<td class="text-right"><b></b></td>
							<td class="text-right"><b></b></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div> {{-- form-bottom --}}
	</div> {{-- col-md-8 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script type="text/javascript">

    function printData()
	{
	   var divTable = document.getElementById("printTable");

	   newWin= window.open("");
	   newWin.document.write(divTable.outerHTML);
	   newWin.print();
	   newWin.close();
	}

	$('#btn_print').on('click',function(){
		printData();
	});


	$('.view-subscribers').on('click',function(){

		var org_url = '{{ route('collector.daily.subscriber') }}';
		var report_date = '{{ $org_date }}';

    	var collector = $(this).data("collector");
    	var target = '_blank';

    	var url = org_url+'?collector='+collector+'&date='+report_date;

    	window.open(url, target);
	});

	$(function() {

		$("#report_date").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'M/dd/yy',
			yearRange: '1950:2050'
		});
	});

</script>
@endsection
