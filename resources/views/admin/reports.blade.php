@extends('layouts.main')
@section('title', '| Reports')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12">
		{{-- <button id="btn_print" class="btn btn-danger form-control">Print</button> --}}
		<a href="{{ route('export.billing') }}" class="btn btn-danger form-control">Download</a>
	</div>
</div>
<br>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="form-top">
			<div class="form-daterange">
				<div class="form-group">
					<h3 class="text-center">Total Arrears: &#8369; {{ number_format($total_arrears,2) }}</h3>
				</div>
			</div>
		</div> {{-- form-top --}}
	</div>
</div>
<br>
<div class="row" id="printTable">
	<div class="col-md-6 col-sm-6 mrgbot20">
		<div class="form-top">
			<div class="form-top-left">
				<h3>BroadCable Communications Inc.</h3>
				<p>Active Subscriber</p>
				<h4>Arrears</h4>
			</div>
			<div class="form-top-right">
				<i class="fa fa-bar-chart"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<div class="table-responsive">
				<table border="1" cellpadding="10" class="table white-text">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Address</th>
							<th>Contact No.</th>
							<th>Balance</th>
						</tr>
					</thead>
					<tbody>
						@foreach($active_subscribers as $subscriber)
						<tr>
							<td>{{ $subscriber->id }}</td>
							<td>{{ $subscriber->name }} {{ $subscriber->last_name }}</td>
							<td>{{ $subscriber->address }}</td>
							<td>{{ $subscriber->contact_number }}</td>
							<td>{{ number_format($subscriber->billings->sum('balance'),2) }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			<div class="text-center">{{ $active_subscribers->appends(['active' => $active_subscribers->currentPage()])->links() }}</div>
			</div>
		</div> {{-- form-bottom --}}
	</div> {{-- col-md-3 --}}
	<div class="col-md-6 col-sm-6">
		<div class="form-top">
			<div class="form-top-left">
				<h3>BroadCable Communications Inc.</h3>
				<p>Inactive Subscriber</p>
				<h4>Arrears</h4>
			</div>
			<div class="form-top-right">
				<i class="fa fa-bar-chart"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<div class="table-responsive">
				<table border="1" cellpadding="10" class="table white-text">
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Address</th>
							<th>Contact No.</th>
							<th>Balance</th>
						</tr>
					</thead>
					<tbody>
						@foreach($inactive_subscribers as $subscriber)
						<tr>
							<td>{{ $subscriber->id }}</td>
							<td>{{ $subscriber->name }}</td>
							<td>{{ $subscriber->address }}</td>
							<td>{{ $subscriber->contact_number }}</td>
							<td>{{ number_format($subscriber->billings->sum('balance'),2) }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			<div class="text-center">{{ $inactive_subscribers->appends(['inactive' => $inactive_subscribers->currentPage()])->links() }}</div>
			</div>
		</div> {{-- form-bottom --}}
	</div> {{-- col-md-6 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script type="text/javascript">

    function printData()
	{
	   var divTable = document.getElementById("printTable");

	   newWin= window.open("");
	   newWin.document.write(divTable.outerHTML);
	   newWin.print();
	   newWin.close();
	}

	$('#btn_print').on('click',function(){
		printData();
	});

</script>
@endsection
