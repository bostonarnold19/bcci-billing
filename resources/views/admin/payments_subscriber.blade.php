@extends('layouts.main')
@section('title', '| Payments')
@section('content')
@include('includes._modal_picture')
<div class="row">
	<div class="col-md-2 col-sm-2">
		@include('includes._side_panel')
	</div> {{-- col-md-2 --}}
	<div class="col-md-10 col-sm-10">
		<div class="form-top">
			<div class="form-top-left">
				<h3>Payment History</h3>
				<p>Name: <i>{{ $subscriber->name }} {{ $subscriber->last_name }}</i></p>
				<p>Address: <i>{{ $subscriber->address }}</i></p>
				<p>Contact Number: <i>{{ $subscriber->contact_number }}</i></p>
				<p>Subscription Rate: <i>{{ $subscriber->subscription->rate }}</i></p>
				<p>Installation Date: <i>{{ empty($subscriber->date_of_installation) ? '' : Carbon\Carbon::parse($subscriber->date_of_installation)->format('M d, Y') }}</i></p>
				<hr>
				@if($subscriber->billings()->first())
				<p><b>Due Date: <i>{{  Carbon\Carbon::parse($subscriber->billings->sortBy('billing_date')->last()->billing_date)->format('M d, Y') }}</i></b></p>
				<p><b>Total Amount Due: <i>{{  $subscriber->billings->sum('balance') }}</i></b></p>
				@else
				<div class="alert alert-danger">
					<div>This is an initial input user. Please see the index card of this user and input the totality of the balances.</div>
				</div>
				@endif
			</div>
			<div class="form-top-right">
				<i class="fa fa-history fa-spin"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<table class="table white-text">
				<thead>
					<tr>
						<th width="120">Date Paid</th>
						<th>Billing Period</th>
						<th width="130">Official Receipt</th>
						<th width="120">Amount Paid</th>
						<th width="120">Collector</th>
					</tr>
				</thead>
				<tbody>
					@foreach($payments as $payment)
					{{-- @if($payment->billings()->first()) --}}
					<tr>
						<td>{{ Carbon\Carbon::parse($payment->date_paid)->format('M d, Y') }}</td>
						<td>
							<?php
foreach ($payment->billings as $billing) {
    echo Carbon\Carbon::parse($billing->billing_date)->format('M d, Y') . ',';
}

?>
						</td>
						<td>{{ $payment->official_receipt }}</td>
						<td>{{ $payment->amount_paid }}</td>
						<td>{{ $payment->collector}}</td>
					</tr>
					{{-- @endif --}}
					@endforeach
				</tbody>
			</table>
		</div> {{-- form-bottom --}}
	</div> {{-- col-md-10 --}}
</div> {{-- row --}}
@endsection
