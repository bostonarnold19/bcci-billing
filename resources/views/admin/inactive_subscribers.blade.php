@extends('layouts.main')
@section('title', '| Inactive Subscribers')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="form-top">
			<div class="form-top-left">
				<h3>Inactive Subscribers</h3>
				<p>Number of Inactive Subscriber: <i>{{ $count }}</i></p>
				<a href="{{ route('dashboard') }}" class="btn btn-info">View active subscribers</a>
			</div>
			<div class="form-top-right">
				<i class="fa fa-user"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<form action="{{ route('inactive.subscribers') }}" method="get">
				<div class="input-group input-group-lg">
					<span class="input-group-addon search-button-custom">
						<i class="fa fa-search" aria-hidden="true"></i>
					</span>
					<input type="text" autocomplete="off" name="search" class="search-input-custom form-control">
					<input type="submit" class="hidden">
				</div>
			</form>
			<br>
			@if($subscribers->count() != 0)
			<div class="table-responsive">
				<table class="table white-text">
					<thead>
						<tr>
							<th>ID</th>
							<th width="250">Name</th>
							<th width="500">Address</th>
							<th>Contact No.</th>
							<th>Disconnection Date</th>
							<th>Balance</th>
							<th>View</th>
						</tr>
					</thead>
					<tbody>
						@foreach($subscribers as $subscriber)
						<tr>
							<td>{{ $subscriber->id }}</td>
							<td>{{ $subscriber->name }} {{ $subscriber->last_name }}</td>
							<td>{{ $subscriber->address }}</td>
							<td>{{ $subscriber->contact_number }}</td>
							<td>{{ date('M d, Y g:i A',strtotime($subscriber->updated_at->timezone('Asia/Manila'))) }}</td>
							<td>{{ $subscriber->billings->sum('balance') }}</td>
							<td>
								<a class="btn btn-info" href="{{ route('billings.subscriber', $subscriber->id) }}">View</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="text-center">{{ $subscribers->appends(Request::only('search'))->render() }}</div>
			@else
			<div class="text-center">
				<h3 class="white-text">No result found..</h3>
			</div>
			@endif
		</div> {{-- form-bottom --}}
	</div> {{-- col-md-10 --}}
</div> {{-- row --}}
@endsection
