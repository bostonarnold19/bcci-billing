@extends('layouts.main')
@section('title', '| Dashboard')
@section('content')
<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="form-top">
			<div class="form-top-left">
				<h3>Active Subscribers</h3>
				<p>Number of Subscriber: <i>{{ $count }}</i></p>
				<a href="{{ route('inactive.subscribers') }}" class="btn btn-info">View inactive subscribers</a>
				<a href="{{ route('add.subscriber') }}" class="btn btn-success">Add new subscriber</a>
				<a href="{{ route('add.old.subscriber') }}" class="btn btn-danger">Add old subscriber (Inactive/disconnected subscriber)</a>
			</div>
			<div class="form-top-right">
				<i class="fa fa-user"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<form action="{{ route('dashboard') }}" method="get">
				<div class="input-group input-group-lg">
					<span class="input-group-addon search-button-custom">
						<i class="fa fa-search" aria-hidden="true"></i>
					</span>
					<input type="text" autocomplete="off" name="search" placeholder="Search By Name or Address" class="search-input-custom form-control">
					<input type="submit" class="hidden">
				</div>
			</form>
			<br>
			@if($subscribers->count() != 0)
			<div class="table-responsive">
				<table class="table white-text">
					<thead>
						<tr>
							<th>ID</th>
							<th width="250">Last Name</th>
							<th width="250">First Name</th>
							<th width="500">Address</th>
							<th>Contact No.</th>
							<th>Latest Due</th>
							<th>Balance</th>
							{{-- <th>Pay</th> --}}
							<th>View</th>
						</tr>
					</thead>
					<tbody>
						@foreach($subscribers as $subscriber)
						<tr>
							<td>{{ $subscriber->id }}</td>
							<td>{{ $subscriber->last_name }}</td>
							<td>{{ $subscriber->name }}</td>
							<td>{{ $subscriber->address }}</td>
							<td>{{ $subscriber->contact_number }}</td>
							@if($subscriber->billings()->first())
							<td>{{ Carbon\Carbon::parse($subscriber->billings->sortBy('billing_date')->last()->billing_date)->format('M d, Y') }}</td>
							@else
							<td>No Billing</td>
							@endif
							<td>{{ $subscriber->billings->sum('balance') }}</td>
							{{-- <td>
								<button onclick="pay('{{$subscriber->id}}')" class="btn btn-danger" data-toggle="modal" data-target="#pay">Pay</button>
							</td> --}}
							<td>
								<a class="btn btn-info" href="{{ route('billings.subscriber', $subscriber->id) }}">View</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="text-center">{{ $subscribers->appends(Request::only('search'))->render() }}</div>
			@else
			<div class="text-center">
				<h3 class="white-text">No result found..</h3>
			</div>
			@endif
		</div> {{-- form-bottom --}}
	</div> {{-- col-md-10 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script>

	$(function() {

		$("#datepay").keypress(function(event) {
			event.preventDefault();
		});

		$("#datepay").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'M/dd/yy',
			yearRange: '1950:2050'
		});
	});

</script>
@endsection
