@extends('layouts.main')
@section('title', '| Billings')
@section('content')
@include('includes._modal_fees')
@include('includes._modal_disconnection')
@include('includes._modal_picture')
@include('includes._modal_payment')
@include('includes._modal_reconnection')
@include('includes._modal_add_billing')
<div class="row">
	<div class="col-md-2 col-sm-2">
		@include('includes._side_panel')
	</div> {{-- col-md-2 --}}
	<div class="col-md-10 col-sm-10">
		<div class="form-top">
			<div class="form-top-left">
				<h3>Billing History</h3>
				<p>Name: <i>{{ $subscriber->name }} {{ $subscriber->last_name }}</i></p>
				<p>Address: <i>{{ $subscriber->address }}</i></p>
				<p>Contact Number: <i>{{ $subscriber->contact_number }}</i></p>
				<p>Subscription Rate: <i>{{ $subscriber->subscription->rate }}</i></p>
				@if(strtotime($subscriber->date_of_installation))
				<p>Installation Date: <i>{{ empty($subscriber->date_of_installation) ? '' : Carbon\Carbon::parse($subscriber->date_of_installation)->format('M d, Y') }}</i></p>
				@endif
				@if($subscriber->billings()->first())
				@if($subscriber->deposit->amount != 0)
				<p>Deposit: <i>{{ $subscriber->deposit->amount }}</i></p>
				@endif
				@if($subscriber->status == 'Active')
				<button data-toggle="modal" data-target="#disconnect" class="btn btn-danger">Disconnect</button>
				@else
				<button data-toggle="modal" data-target="#reconnect" class="btn btn-warning">Reconnect</button>
				@endif
				<button class="btn btn-success" data-toggle="modal" data-target="#pay">Pay Balance</button>
				<hr>
				@if($subscriber->deposit->amount != 0)
				<form action="{{ route('post.deposit') }}" method="post">
					<input type="hidden" value="{{ $subscriber->id }}" name="id">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button class="btn btn-primary" type="submit">Use Deposit</button>
				</form>
				@endif
				<br>
				<p><b>Lastest Due Date: <i>{{ Carbon\Carbon::parse($subscriber->billings->sortBy('billing_date')->last()->billing_date)->format('M d, Y')  }}</i></b></p>
				<p><b>Total Amount Due: <i>{{ $subscriber->billings->sum('balance') }} </i></b></p>
				@else
				<hr>
				<div class="alert alert-danger">
					<div>This is an initial input user. Please see the index card of this user and input the totality of the balances.</div>
				</div>
				@endif
				<button class="btn btn-success" data-toggle="modal" data-target="#billing-add">Manual Add Billing</button>
			</div>
			<div class="form-top-right">
				<i class="fa fa-history fa-spin"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<table class="table white-text">
				<thead>
					<tr>
						<th>Billing No.</th>
						<th>Billing Date</th>
						{{-- <th>Billing Period Start</th> --}}
						{{-- <th>Billing Period End</th> --}}
						<th>Due Date</th>
						<th>Amount Due</th>
						<th>Balance</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $count = $subscriber->billings->count();?>
					@forelse($billings as $billing)
					<tr>
						<td>{{ $count }}</td>
						<td>{{ Carbon\Carbon::parse($billing->billing_date)->format('M d, Y') }}</td>
						{{-- <td>{{ Carbon\Carbon::parse($billing->billing_period_start)->format('M d, Y') }}</td> --}}
						{{-- <td>{{ Carbon\Carbon::parse($billing->billing_period_end)->format('M d, Y') }}</td> --}}
						<td>{{ Carbon\Carbon::parse($billing->due_date)->format('M d, Y') }}</td>
						<td>{{ $billing->amount_due }}</td>
						<td>{{ $billing->balance }}</td>
						<td>{{ $billing->status }}</td>
						<td>
							<button onclick="view_fees('{{$billing->id}}')" class="btn btn-info" data-toggle="modal" data-target="#fee">View</button>
							{{-- 			@if($firstbilling->id == $billing->id)
							@if($billing->status == 'Pending')
							@if($subscriber->subscription->rate != $billing->amount_due && $billing->amount_due != 0)
							<button class="btn btn-danger" data-toggle="modal" data-target="#billing">Editable(Billing)</button>
							@endif
							@endif
							@endif --}}
							<button class="btn btn-warning" data-toggle="modal" data-target="#billing-{{$billing->id}}">Edit Billing</button>
							<button type="submit" class="btn btn-danger" form="billingform-{{$billing->id}}">Delete</button>
							<form id="billingform-{{$billing->id}}" onsubmit="return confirm('Are you sure you want to delete this ?');" method="POST" action="{{ route('billing.destroy', $billing->id) }}">
								{{ csrf_field() }}
								{{ method_field('DELETE') }}
							</form>
						</td>
					</tr>
					@include('includes._modal_editable_billing')
					<?php $count--;?>
					@empty
					@endforelse
				</tbody>
			</table>
		</div> {{-- form-bottom --}}
	</div> {{-- col-md-10 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script type="text/javascript">

		  $(function() {

    $("#edit_billing_date").keypress(function(event) {
      event.preventDefault();
    });

    $("#edit_billing_date").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'M/dd/yy',
      yearRange: '1950:2050'
    });

  });

	  $(function() {

    $("#billing_date").keypress(function(event) {
      event.preventDefault();
    });

    $("#billing_date").datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: 'M/dd/yy',
      yearRange: '1950:2050'
    });

  });

	$(function() {

		$("#datepay, #datereconnect").keypress(function(event) {
			event.preventDefault();
		});

		$("#datepay, #datereconnect").datepicker({
			changeMonth: true,
			changeYear: true,
			dateFormat: 'M/dd/yy',
			yearRange: '1950:2050'
		});
	});

	var billingFee = "{{ route('billing.fees') }}";

	function view_fees(id)
	{
		$.ajax({
		url: billingFee,
		type:"GET",
		data: {id:id},
		success: function(res) {
			// console.log(res);
			if(res.transfer_fee == null) {
				res.transfer_fee = 0;
			}
			if(res.installation_fee == null) {
				res.installation_fee = 0;
			}
			if(res.reconnection_fee == null) {
				res.reconnection_fee = 0;
			}
			if(res.subscription_fee == null) {
				res.subscription_fee = 0;
			}
			if(res.wire_fee == null) {
				res.wire_fee = 0;
			}
			if(res.wire_length == null) {
				res.wire_length = 0;
			}
			if(res.disconnection_fee == null) {
				res.disconnection_fee = 0;
			}
			if(res.deposit_fee == null) {
				res.deposit_fee = 0;
			}
			if(res.discount == null) {
				res.discount = 0;
			}
			$("#installation_fee").text(res.installation_fee);
			$("#disconnection_fee").text(res.disconnection_fee);
			$("#subscription_fee").text(res.subscription_fee);
			$("#transfer_fee").text(res.transfer_fee);
			$("#deposit_fee").text(res.deposit_fee);
			$("#reconnection_fee").text(res.reconnection_fee);
			$("#wire_fee").text(res.wire_fee);
			$("#discount").text(res.discount);
			$("#wire_length").text(res.wire_length + ' ft.');
			$("#total").text(res.subscription_fee + res.wire_fee + res.installation_fee + res.transfer_fee + res.reconnection_fee + res.deposit_fee + res.disconnection_fee - res.discount);
			}
		})
	}
</script>
@endsection
