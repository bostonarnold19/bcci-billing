@extends('layouts.main')
@section('title', '| Collector')
@section('content')
<div class="row">
	<div class="col-md-5 col-sm-5 mrgbot20">
		<div class="form-top">
			<div class="form-daterange">
				<h3>Add a new Collector</h3>
				<hr>
				<form id="form_collector" role="form" action="{{ route('post.collector') }}" method="post">
					<div class="form-group">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input name="name" required type="text" class="readonly-bg-custom bci-custom-input form-control" placeholder="Collector's name.." autocomplete="off">
					</div>
					<div class="form-group">
						<button type="submit" class="bci-custom-button">Submit</button>
					</div>
				</form>
				<div class="form-group">
					<button type="submit" id="btn_add" class="bci-custom-button"><i class="fa fa-plus"></i></button>
				</div>
			</div> {{-- form-daterange --}}
		</div> {{-- form-top --}}
		<br>
	</div> {{-- col-md-3 --}}
	<div class="col-md-7 col-sm-7">
		<div class="form-top">
			<div class="form-top-left">
				<h3>BroadCable Communications Inc.</h3>
				<h4>Collectors</h4>
			</div>
			<div class="form-top-right">
				<i class="fa fa-user"></i>
			</div>
		</div> {{-- form-top --}}
		<div class="form-bottom">
			<div class="table-responsive">
				<table border="1" cellpadding="10" class="table white-text">
					<thead>
						<tr>
							<th>Name</th>
							<th width="100">Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($collectors->users as $collector)
						<tr>
							<td>{{ $collector->name }}</td>
							<td>
								<form action="{{ route('patch.collector.remove') }}" method="post">
									<input type="hidden" name="_method" value="PATCH">
									<input type="hidden" name="collector_id" value="{{ $collector->id }}">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<button type="submit" class="btn btn-danger">Remove</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div> {{-- form-bottom --}}
	</div> {{-- col-md-6 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script>

	$('#form_collector').hide();
	$('#btn_add').click(function(){
		$('#form_collector').show();
		$('#btn_add').hide();
	});

</script>
@endsection
