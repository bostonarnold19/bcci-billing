@extends('layouts.main')
@section('title', '| Profile')
@section('content')
@include('includes._modal_transfer')
@include('includes._modal_picture')
<div class="row">
    <div class="col-md-2 col-sm-2">
        @include('includes._side_panel')
    </div> {{-- col-md-2 --}}
    <div class="col-md-10 col-sm-10">
        <div class="form-top">
            <div class="form-top-left">
                <h3>Subscriber Settings</h3>
                <hr>
                <button data-toggle="modal" data-target="#transfer" type="button" class="btn btn-danger">Generate Transfer Billing</button>
                <button type="submit" class="btn btn-danger" form="deleteSubscriber">Delete This Subscriber</button>
                <form id="deleteSubscriber" onsubmit="return confirm('Are you sure you want to delete this ?');" method="POST" action="{{ route('user-subscriber.destroy', $subscriber->id) }}">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>
            </div>
            <div class="form-top-right">
                <i class="fa fa-cog fa-spin"></i>
            </div>
        </div> {{-- form-top --}}
        <div class="form-bottom">
            <div class="row">
                <div class="col-md-8">
                    <div class="row" id="show_subscriber_name">
                        <div class="col-md-11">
                            <div class="form-inline">
                                <div class="form-group">
                                    <p class="white-text"><b>Name:</b></p>
                                    <p id="subscriber_name" class="white-text"><i>{{ $subscriber->name }}</i></p>
                                    <input id="input_name" type="text" class="form-control" value="{{ $subscriber->name }}">
                                    <button id="btn_name_update" type="button" class="btn btn-info">Update</button>
                                    <button id="btn_name_cancel" type="button" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a class="custom-href" id="subscriber_name_edit">Edit</a>
                        </div>
                    </div>
                    <hr>
                    <div class="row" id="show_subscriber_address">
                        <div class="col-md-11">
                            <div class="form-inline">
                                <div class="form-group">
                                    <p class="white-text"><b>Address:</b></p>
                                    <p id="subscriber_address" class="white-text"><i>{{ $subscriber->address }}</i></p>
                                    <textarea noresize cols="50" class="form-control" id="input_address">{{ $subscriber->address }}</textarea>
                                    <br>
                                    <br>
                                    <button id="btn_address_update" type="button" class="btn btn-info">Update</button>
                                    <button id="btn_address_cancel" type="button" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a class="custom-href" id="subscriber_address_edit">Edit</a>
                        </div>
                    </div>
                    <hr>
                    <div class="row" id="show_subscriber_contact_number">
                        <div class="col-md-11">
                            <div class="form-inline">
                                <div class="form-group">
                                    <p class="white-text"><b>Contact Number:</b></p>
                                    <p id="subscriber_contact_number" class="white-text"><i>{{ $subscriber->contact_number }}</i></p>
                                    <input id="input_contact_number" type="text" class="form-control" value="{{ $subscriber->contact_number }}">
                                    <button id="btn_contact_number_update" type="button" class="btn btn-info">Update</button>
                                    <button id="btn_contact_number_cancel" type="button" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a class="custom-href" id="subscriber_contact_number_edit">Edit</a>
                        </div>
                    </div>
                    <hr>
                    <div class="row" id="show_subscriber_date_of_installation">
                        <div class="col-md-11">
                            <div class="form-inline">
                                <div class="form-group">
                                    <p class="white-text"><b>Date of Installation:</b></p>
                                    <p id="subscriber_date_of_installation" class="white-text"><i>{{ empty($subscriber->date_of_installation) ? '' : Carbon\Carbon::parse($subscriber->date_of_installation )->format('M/d/Y') }}</i></p>
                                    <input id="input_date_of_installation" type="text" class="form-control" value="{{ empty($subscriber->date_of_installation) ? '' : Carbon\Carbon::parse($subscriber->date_of_installation )->format('M/d/Y') }}">
                                    <button id="btn_date_of_installation_update" type="button" class="btn btn-info">Update</button>
                                    <button id="btn_date_of_installation_cancel" type="button" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a class="custom-href" id="subscriber_date_of_installation_edit">Edit</a>
                        </div>
                    </div>
                    <hr>
                    <div class="row" id="show_subscriber_rate">
                        <div class="col-md-11">
                            <div class="form-inline">
                                <div class="form-group">
                                    <p class="white-text"><b>Subscription Rate:</b></p>
                                    <p id="subscriber_rate" class="white-text"><i>{{ $subscriber->subscription->rate }}</i></p>
                                    <input id="input_rate" type="text" class="form-control" value="{{ $subscriber->subscription->rate }}">
                                    <button id="btn_rate_update" type="button" class="btn btn-info">Update</button>
                                    <button id="btn_rate_cancel" type="button" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a class="custom-href" id="subscriber_rate_edit">Edit</a>
                        </div>
                    </div>
                    <br>
                </div> {{-- col-md-8 --}}
            </div> {{-- row --}}
        </div> {{-- form-bottom --}}
    </div> {{-- col-md-10 --}}
</div> {{-- row --}}
@endsection
@section('scripts')
<script>

    $(function() {
        $("#billing_date, #input_date_of_installation").keypress(function(event) {
            event.preventDefault();
        });
        $("#billing_date, #input_date_of_installation").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'M/dd/yy',
            yearRange: '1950:2050'
        });
    });

    $(document).ready(function(){
        //Date of Installation
        $('#input_date_of_installation').hide();
        $('#btn_date_of_installation_cancel').hide();
        $('#btn_date_of_installation_update').hide();
        $('#subscriber_date_of_installation_edit').click(function(){
            $('#subscriber_date_of_installation').hide();
            $('#subscriber_date_of_installation_edit').hide();
            $('#input_date_of_installation').show();
            $('#btn_date_of_installation_cancel').show();
            $('#btn_date_of_installation_update').show();
            $('#input_name').hide();
            $('#btn_name_cancel').hide();
            $('#btn_name_update').hide();
            $('#input_rate').hide();
            $('#btn_rate_cancel').hide();
            $('#btn_rate_update').hide();
            $('#input_contact_number').hide();
            $('#btn_contact_number_cancel').hide();
            $('#btn_contact_number_update').hide();
            $('#input_address').hide();
            $('#btn_address_cancel').hide();
            $('#btn_address_update').hide();
            $('#subscriber_contact_number_edit').show();
            $('#subscriber_rate_edit').show();
            $('#subscriber_name_edit').show();
            $('#subscriber_address_edit').show();
            $('#subscriber_contact_number').show();
            $('#subscriber_rate').show();
            $('#subscriber_name').show();
            $('#subscriber_address').show();
            $('#input_contact_number').val(contact_numberVal);
            $('#input_contact_number').css('border-color','');
            $('#input_rate').val(rateVal);
            $('#input_rate').css('border-color','');
            $('#input_name').val(nameVal);
            $('#input_name').css('border-color','');
            $('#input_address').val(addressVal);
            $('#input_address').css('border-color','');
        });
        $('#btn_date_of_installation_cancel').click(function(){
            $('#subscriber_date_of_installation').show();
            $('#subscriber_date_of_installation_edit').show();
            $('#input_date_of_installation').hide();
            $('#btn_date_of_installation_cancel').hide();
            $('#btn_date_of_installation_update').hide();
            $('#input_date_of_installation').val(date_of_installationVal);
            $('#input_date_of_installation').css('border-color','');
        });

        //Address
        $('#input_address').hide();
        $('#btn_address_cancel').hide();
        $('#btn_address_update').hide();
        $('#subscriber_address_edit').click(function(){
            $('#subscriber_address').hide();
            $('#subscriber_address_edit').hide();
            $('#input_address').show();
            $('#btn_address_cancel').show();
            $('#btn_address_update').show();
            $('#input_name').hide();
            $('#btn_name_cancel').hide();
            $('#btn_name_update').hide();
            $('#input_rate').hide();
            $('#btn_rate_cancel').hide();
            $('#btn_rate_update').hide();
            $('#input_contact_number').hide();
            $('#btn_contact_number_cancel').hide();
            $('#btn_contact_number_update').hide();
            $('#input_date_of_installation').hide();
            $('#btn_date_of_installation_cancel').hide();
            $('#btn_date_of_installation_update').hide();
            $('#subscriber_contact_number_edit').show();
            $('#subscriber_rate_edit').show();
            $('#subscriber_name_edit').show();
            $('#subscriber_date_of_installation_edit').show();
            $('#subscriber_contact_number').show();
            $('#subscriber_rate').show();
            $('#subscriber_name').show();
            $('#subscriber_date_of_installation').show();
            $('#input_contact_number').val(contact_numberVal);
            $('#input_contact_number').css('border-color','');
            $('#input_rate').val(rateVal);
            $('#input_rate').css('border-color','');
            $('#input_name').val(nameVal);
            $('#input_name').css('border-color','');
            $('#input_date_of_installation').val(date_of_installationVal);
            $('#input_date_of_installation').css('border-color','');
        });
        $('#btn_address_cancel').click(function(){
            $('#subscriber_address').show();
            $('#subscriber_address_edit').show();
            $('#input_address').hide();
            $('#btn_address_cancel').hide();
            $('#btn_address_update').hide();
            $('#input_address').val(addressVal);
            $('#input_address').css('border-color','');
        });

        //Contact Number
        $('#input_contact_number').hide();
        $('#btn_contact_number_cancel').hide();
        $('#btn_contact_number_update').hide();
        $('#subscriber_contact_number_edit').click(function(){
            $('#subscriber_contact_number').hide();
            $('#subscriber_contact_number_edit').hide();
            $('#input_contact_number').show();
            $('#btn_contact_number_cancel').show();
            $('#btn_contact_number_update').show();
            $('#input_address').hide();
            $('#btn_address_cancel').hide();
            $('#btn_address_update').hide();
            $('#input_name').hide();
            $('#btn_name_cancel').hide();
            $('#btn_name_update').hide();
            $('#input_rate').hide();
            $('#btn_rate_cancel').hide();
            $('#btn_rate_update').hide();
            $('#input_date_of_installation').hide();
            $('#btn_date_of_installation_cancel').hide();
            $('#btn_date_of_installation_update').hide();
            $('#subscriber_address_edit').show();
            $('#subscriber_rate_edit').show();
            $('#subscriber_name_edit').show();
            $('#subscriber_date_of_installation_edit').show();
            $('#subscriber_address').show();
            $('#subscriber_rate').show();
            $('#subscriber_name').show();
            $('#subscriber_date_of_installation').show();
            $('#input_address').val(addressVal);
            $('#input_address').css('border-color','');
            $('#input_rate').val(rateVal);
            $('#input_rate').css('border-color','');
            $('#input_name').val(nameVal);
            $('#input_name').css('border-color','');
            $('#input_date_of_installation').val(date_of_installationVal);
            $('#input_date_of_installation').css('border-color','');
        });
        $('#btn_contact_number_cancel').click(function(){
            $('#subscriber_contact_number').show();
            $('#subscriber_contact_number_edit').show();
            $('#input_contact_number').hide();
            $('#btn_contact_number_cancel').hide();
            $('#btn_contact_number_update').hide();
            $('#input_contact_number').val(contact_numberVal);
            $('#input_contact_number').css('border-color','');
        });

        //Rate
        $('#input_rate').hide();
        $('#btn_rate_cancel').hide();
        $('#btn_rate_update').hide();
        $('#subscriber_rate_edit').click(function(){
            $('#subscriber_rate').hide();
            $('#subscriber_rate_edit').hide();
            $('#input_rate').show();
            $('#btn_rate_cancel').show();
            $('#btn_rate_update').show();
            $('#input_address').hide();
            $('#btn_address_cancel').hide();
            $('#btn_address_update').hide();
            $('#input_contact_number').hide();
            $('#btn_contact_number_cancel').hide();
            $('#btn_contact_number_update').hide();
            $('#input_name').hide();
            $('#btn_name_cancel').hide();
            $('#btn_name_update').hide();
            $('#input_date_of_installation').hide();
            $('#btn_date_of_installation_cancel').hide();
            $('#btn_date_of_installation_update').hide();
            $('#subscriber_address_edit').show();
            $('#subscriber_contact_number_edit').show();
            $('#subscriber_name_edit').show();
            $('#subscriber_date_of_installation_edit').show();
            $('#subscriber_address').show();
            $('#subscriber_contact_number').show();
            $('#subscriber_name').show();
            $('#subscriber_date_of_installation').show();
            $('#input_address').val(addressVal);
            $('#input_address').css('border-color','');
            $('#input_name').val(nameVal);
            $('#input_name').css('border-color','');
            $('#input_contact_number').val(contact_numberVal);
            $('#input_contact_number').css('border-color','');
            $('#input_date_of_installation').val(date_of_installationVal);
            $('#input_date_of_installation').css('border-color','');
        });
        $('#btn_rate_cancel').click(function(){
            $('#subscriber_rate').show();
            $('#subscriber_rate_edit').show();
            $('#input_rate').hide();
            $('#btn_rate_cancel').hide();
            $('#btn_rate_update').hide();
            $('#input_rate').val(rateVal);
            $('#input_rate').css('border-color','');
        });

        //Name
        $('#input_name').hide();
        $('#btn_name_cancel').hide();
        $('#btn_name_update').hide();
        $('#subscriber_name_edit').click(function(){
            $('#subscriber_name').hide();
            $('#subscriber_name_edit').hide();
            $('#input_name').show();
            $('#btn_name_cancel').show();
            $('#btn_name_update').show();
            $('#input_contact_number').hide();
            $('#btn_contact_number_cancel').hide();
            $('#btn_contact_number_update').hide();
            $('#input_rate').hide();
            $('#btn_rate_cancel').hide();
            $('#btn_rate_update').hide();
            $('#input_address').hide();
            $('#btn_address_cancel').hide();
            $('#btn_address_update').hide();
            $('#input_date_of_installation').hide();
            $('#btn_date_of_installation_cancel').hide();
            $('#btn_date_of_installation_update').hide();
            $('#subscriber_address_edit').show();
            $('#subscriber_contact_number_edit').show();
            $('#subscriber_rate_edit').show();
            $('#subscriber_date_of_installation_edit').show();
            $('#subscriber_address').show();
            $('#subscriber_contact_number').show();
            $('#subscriber_rate').show();
            $('#subscriber_date_of_installation').show();
            $('#input_address').val(addressVal);
            $('#input_address').css('border-color','');
            $('#input_contact_number').val(contact_numberVal);
            $('#input_contact_number').css('border-color','');
            $('#input_rate').val(rateVal);
            $('#input_rate').css('border-color','');
            $('#input_date_of_installation').val(date_of_installationVal);
            $('#input_date_of_installation').css('border-color','');
        });
        $('#btn_name_cancel').click(function(){
            $('#subscriber_name').show();
            $('#subscriber_name_edit').show();
            $('#input_name').hide();
            $('#btn_name_cancel').hide();
            $('#btn_name_update').hide();
            $('#input_name').val(nameVal);
            $('#input_name').css('border-color','');
        });
    });

    var RouteSubscriberName = "{{ route('patch.subscriber.name') }}";
    var RouteSubscriberContactNumber = "{{ route('patch.subscriber.contact_number') }}";
    var RouteSubscriberRate = "{{ route('patch.subscriber.rate') }}";
    var RouteSubscriberAddress = "{{ route('patch.subscriber.address') }}";
    var RouteSubscriberDateOfInstallation = "{{ route('patch.subscriber.date_of_installation') }}";
    var id = '{{ $subscriber->id }}';
    var nameVal = '{{ $subscriber->name }}';
    var rateVal = '{{ $subscriber->subscription->rate }}';
    var contact_numberVal = '{{ $subscriber->contact_number }}';
    var addressVal = '{{ $subscriber->address }}';
    var date_of_installationVal = '{{ $subscriber->date_of_installation }}';


    var token = '{{ Session::token() }}';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    });

    $(document).ready(function(){

        $('#btn_contact_number_update').on('click', function (){

            var contact_number = $('#input_contact_number').val();

            $.ajax({
                method: "PATCH",
                url: RouteSubscriberContactNumber,
                data: {
                    id:id,
                    subscriber_contact_number:contact_number,
                    _token:token
                }
            })
            .done(function(res) {
                // console.log(res);
                $('#input_contact_number').hide();
                $('#input_contact_number').css('border-color','');
                $('#input_contact_number').val(res.contact_number);
                $('#btn_contact_number_cancel').hide();
                $('#btn_contact_number_update').hide();
                $('#subscriber_contact_number').text(res.contact_number);
                $('#subscriber_contact_number').show();
                $('#subscriber_contact_number_edit').show();

                successHtml = '<div class="alert alert-success"><div>'+ res.success + '</div></div>';
                $('#ajax-message').html(successHtml);
            })
            .error(function(res) {
                var errors = res.responseJSON;
                // console.log(errors);
                errorsHtml = '';
                $.each(errors.subscriber_contact_number, function(key, value) {
                    errorsHtml += '<div class="alert alert-danger"><div>'+ value + '</div></div>';
                });

                $('#input_contact_number').css('border-color','red');
                $('#ajax-message').html(errorsHtml);
            });
        });

        $('#btn_date_of_installation_update').on('click', function (){

            var date_of_installation = $('#input_date_of_installation').val();

            $.ajax({
                method: "PATCH",
                url: RouteSubscriberDateOfInstallation,
                data: {
                    id:id,
                    subscriber_date_of_installation:date_of_installation,
                    _token:token
                }
            })
            .done(function(res) {
                // console.log(res);
                $('#input_date_of_installation').hide();
                $('#input_date_of_installation').css('border-color','');
                $('#input_date_of_installation').val(res.date_of_installation);
                $('#btn_date_of_installation_cancel').hide();
                $('#btn_date_of_installation_update').hide();
                $('#subscriber_date_of_installation').text(res.date_of_installation);
                $('#subscriber_date_of_installation').show();
                $('#subscriber_date_of_installation_edit').show();

                successHtml = '<div class="alert alert-success"><div>'+ res.success + '</div></div>';
                $('#ajax-message').html(successHtml);
            })
            .error(function(res) {
                var errors = res.responseJSON;
                // console.log(errors);
                errorsHtml = '';
                $.each(errors.subscriber_date_of_installation, function(key, value) {
                    errorsHtml += '<div class="alert alert-danger"><div>'+ value + '</div></div>';
                });

                $('#input_date_of_installation').css('border-color','red');
                $('#ajax-message').html(errorsHtml);
            });
        });

        $('#btn_name_update').on('click', function (){

            var name = $('#input_name').val();

            $.ajax({
                method: "PATCH",
                url: RouteSubscriberName,
                data: {
                    id:id,
                    subscriber_name:name,
                    _token:token
                }
            })
            .done(function(res) {
                // console.log(res);
                $('#input_name').hide();
                $('#input_name').css('border-color','');
                $('#input_name').val(res.name);
                $('#btn_name_cancel').hide();
                $('#btn_name_update').hide();
                $('#subscriber_name_side_panel').text(res.name);
                $('#subscriber_name').text(res.name);
                $('#subscriber_name').show();
                $('#subscriber_name_edit').show();

                successHtml = '<div class="alert alert-success"><div>'+ res.success + '</div></div>';
                $('#ajax-message').html(successHtml);
            })
            .error(function(res) {
                var errors = res.responseJSON;
                // console.log(errors);
                errorsHtml = '';
                $.each(errors.subscriber_name, function(key, value) {
                    errorsHtml += '<div class="alert alert-danger"><div>'+ value + '</div></div>';
                });

                $('#input_name').css('border-color','red');
                $('#ajax-message').html(errorsHtml);
            });
        });

        $('#btn_rate_update').on('click', function (){

            var rate = $('#input_rate').val();

            $.ajax({
                method: "PATCH",
                url: RouteSubscriberRate,
                data: {
                    id:id,
                    subscriber_rate:rate,
                    _token:token
                }
            })
            .done(function(res) {
                // console.log(res);
                $('#input_rate').hide();
                $('#input_rate').css('border-color','');
                $('#input_rate').val(res.rate);
                $('#btn_rate_cancel').hide();
                $('#btn_rate_update').hide();
                $('#subscriber_rate').text(res.rate);
                $('#subscriber_rate').show();
                $('#subscriber_rate_edit').show();

                successHtml = '<div class="alert alert-success"><div>'+ res.success + '</div></div>';
                $('#ajax-message').html(successHtml);
            })
            .error(function(res) {
                var errors = res.responseJSON;
                // console.log(errors);
                errorsHtml = '';
                $.each(errors.subscriber_rate, function(key, value) {
                    errorsHtml += '<div class="alert alert-danger"><div>'+ value + '</div></div>';
                });

                $('#input_rate').css('border-color','red');
                $('#ajax-message').html(errorsHtml);
            });
        });

        $('#btn_address_update').on('click', function (){

            var address = $('#input_address').val();

            $.ajax({
                method: "PATCH",
                url: RouteSubscriberAddress,
                data: {
                    id:id,
                    subscriber_address:address,
                    _token:token
                }
            })
            .done(function(res) {
                // console.log(res);
                $('#input_address').hide();
                $('#input_address').css('border-color','');
                $('#input_address').val(res.address);
                $('#btn_address_cancel').hide();
                $('#btn_address_update').hide();
                $('#subscriber_address').text(res.address);
                $('#subscriber_address').show();
                $('#subscriber_address_edit').show();

                successHtml = '<div class="alert alert-success"><div>'+ res.success + '</div></div>';
                $('#ajax-message').html(successHtml);
            })
            .error(function(res) {
                var errors = res.responseJSON;
                // console.log(errors);
                errorsHtml = '';
                $.each(errors, function(key, value) {
                    errorsHtml += '<div class="alert alert-danger"><div>'+ value + '</div></div>';
                });

                $('#input_address').css('border-color','red');
                $('#ajax-message').html(errorsHtml);
            });

        });

    });
</script>
@endsection
