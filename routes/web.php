<?php
//Auth
Route::get('/', [
    'uses' => 'Auth\LoginController@getLogin',
    'as' => 'login.get',
]);

Route::post('login', [
    'uses' => 'Auth\LoginController@postLogin',
    'as' => 'login.post',
]);

Route::get('logout', [
    'uses' => 'Auth\LoginController@getLogout',
    'as' => 'logout',
]);

Route::group(['prefix' => 'dashboard', 'middleware' => ['role:Super Admin']], function () {
    Route::resource('activity', 'ActivityLogController');

    Route::get('collectors', [
        'uses' => 'SuperAdminController@getCollectors',
        'as' => 'get.collectors',
    ]);

    Route::post('collector', [
        'uses' => 'SuperAdminController@postCollector',
        'as' => 'post.collector',
    ]);

    Route::patch('collector/remove', [
        'uses' => 'SuperAdminController@patchCollectorRemove',
        'as' => 'patch.collector.remove',
    ]);
});


Route::group(['prefix' => 'dashboard', 'middleware' => ['role:Accountant|Super Admin|Collector']], function () {

    Route::get('subscriber/billing/fees', [
        'uses' => 'SuperAdminController@getFees',
        'as' => 'billing.fees',
    ]);

});

Route::group(['prefix' => 'dashboard', 'middleware' => ['role:Accountant|Super Admin']], function () {

    Route::resource('billing', 'BillingController');

    Route::resource('statement', 'StatementOfAccountController');

    Route::resource('user-subscriber', 'SubscriberController');

    Route::get('/', [
        'uses' => 'SuperAdminController@getDashboard',
        'as' => 'dashboard',
    ]);

    Route::get('inactive/subscribers', [
        'uses' => 'SuperAdminController@getInactiveSubscribers',
        'as' => 'inactive.subscribers',
    ]);

    Route::patch('subscriber/disconnect/', [
        'uses' => 'SuperAdminController@patchDisconnectSubscriber',
        'as' => 'disconnect.subscriber',
    ]);

    Route::patch('inactive/subscriber/reconnect', [
        'uses' => 'SuperAdminController@patchReconnectSubscriber',
        'as' => 'reconnect.subscriber',
    ]);

    Route::post('pay', [
        'uses' => 'SuperAdminController@postPay',
        'as' => 'post.pay',
    ]);

    Route::get('add/subscriber', [
        'uses' => 'SuperAdminController@getAddSubscriber',
        'as' => 'add.subscriber',
    ]);

    Route::post('store/subscriber', [
        'uses' => 'SuperAdminController@getStoreSubscriber',
        'as' => 'store.subscriber',
    ]);

    Route::get('subscriber/billings/{id}', [
        'uses' => 'SuperAdminController@getSubscriberBillings',
        'as' => 'billings.subscriber',
    ]);

    Route::get('subscriber/payments/{id}', [
        'uses' => 'SuperAdminController@getSubscriberPayments',
        'as' => 'payments.subscriber',
    ]);

    Route::get('subscriber/profile/{id}', [
        'uses' => 'SuperAdminController@getSubscriberProfile',
        'as' => 'profile.subscriber',
    ]);

    Route::get('subscriber/statement-of-account/{id}', [
        'uses' => 'SuperAdminController@getSubscriberStatementOfAccount',
        'as' => 'soa.subscriber',
    ]);

    Route::patch('subscriber/profile/patch/name', [
        'uses' => 'SuperAdminController@patchSubscriberName',
        'as' => 'patch.subscriber.name',
    ]);

    Route::patch('subscriber/patch/contact_number', [
        'uses' => 'SuperAdminController@patchSubscriberContactNumber',
        'as' => 'patch.subscriber.contact_number',
    ]);

    Route::patch('subscriber/patch/date_of_installation', [
        'uses' => 'SuperAdminController@patchSubscriberDateOfInstallation',
        'as' => 'patch.subscriber.date_of_installation',
    ]);

    Route::patch('subscriber/patch/rate', [
        'uses' => 'SuperAdminController@patchSubscriberRate',
        'as' => 'patch.subscriber.rate',
    ]);

    Route::patch('subscriber/patch/address', [
        'uses' => 'SuperAdminController@patchSubscriberAddress',
        'as' => 'patch.subscriber.address',
    ]);

    Route::patch('subscriber/patch/picture', [
        'uses' => 'SuperAdminController@patchSubscriberPicture',
        'as' => 'patch.subscriber.picture',
    ]);

    Route::post('subscriber/transfer', [
        'uses' => 'SuperAdminController@postSubscriberTransfer',
        'as' => 'post.transfer',
    ]);

    Route::get('add/old-subscriber', [
        'uses' => 'SuperAdminController@getAddOldSubscriber',
        'as' => 'add.old.subscriber',
    ]);

    Route::post('add/old-subscriber', [
        'uses' => 'SuperAdminController@postAddOldSubscriber',
        'as' => 'post.add.old.subscriber',
    ]);

    Route::patch('subscriber/billing/update', [
        'uses' => 'SuperAdminController@patchSubscriberBilling',
        'as' => 'patch.subscriber.billing',
    ]);

    Route::get('collector/daily/report', [
        'uses' => 'SuperAdminController@getCollectorDailyReport',
        'as' => 'collector.daily',
    ]);

    Route::get('collector/monthly/report', [
        'uses' => 'SuperAdminController@getCollectorMonthlyReport',
        'as' => 'collector.monthly',
    ]);

    Route::get('collector/daily/report-subscriber', [
        'uses' => 'SuperAdminController@getSubscribersReportDaily',
        'as' => 'collector.daily.subscriber',
    ]);

    Route::get('collector/monthly/report-subscriber', [
        'uses' => 'SuperAdminController@getSubscribersReportMonthly',
        'as' => 'collector.monthly.subscriber',
    ]);

    Route::get('collector/daily-all/report-subscriber', [
        'uses' => 'SuperAdminController@getAllSubscribersReportDaily',
        'as' => 'collector.daily-all.subscriber',
    ]);

    Route::post('deposit', [
        'uses' => 'SuperAdminController@postDeposit',
        'as' => 'post.deposit',
    ]);

});

//////////////////////COLLECTOR////////////////////////////////
Route::get('collector-dashboard/subscribers', [
    'uses' => 'CollectorController@getSubscriber',
    'as' => 'collector.subscribers',
    'middleware' => ['role:Collector'],
]);

Route::post('collector-dashboard/pay', [
    'uses' => 'CollectorController@pay',
    'as' => 'collector.pay',
    'middleware' => ['role:Collector'],
]);

Route::get('collector-dashboard/thank-you-page/{id}', [
    'uses' => 'CollectorController@ty',
    'as' => 'collector.ty',
    'middleware' => ['role:Collector'],
]);

Route::get('collector-performance/payments', [
    'uses' => 'CollectorController@performance',
    'as' => 'collector-performance.payments',
    'middleware' => ['role:Collector'],
]);

Route::get('export-subscribers', function () {
    ini_set('memory_limit', '-1');
    try {
        $user = auth()->user();

        $active_subscribers = App\Subscriber::where('status', 'active')
            ->where('collector', $user->name)
            ->get();

        $new = [];

        foreach($active_subscribers as $k => $v) {
                if(number_format($v->billings->sum('balance'),2) == '0.00') {
                    continue;
                }
                $new[$k]['id'] = $v->id;
                $new[$k]['name'] = $v->name.' '.$v->last_name;
                $new[$k]['address'] = $v->address;
                $new[$k]['contact_number'] = $v->contact_number;
                $new[$k]['balance'] = number_format($v->billings->sum('balance'),2);
                // $new[$k]['sort_balance'] = $v->billings->sum('balance');
                $new[$k]['last_payment'] = @$v->payments()->latest()->first()->date_paid;
                $new[$k]['collector'] = @$v->collector;
        }

        // usort($new, function($a, $b) {
        //     return $a['sort_balance'] <=> $b['sort_balance'];
        // });

        $list = collect($new);

        // $list = $xlist->transform(function(array $item) {
        //     return array_except($item, ['sort_balance']);
        // });

        return (new Rap2hpoutre\FastExcel\FastExcel($list))->download('subscribers_collector.xlsx');
    } catch (Exception $e) {
        echo 'Message: ' .$e->getMessage();
    }

})->name('export.subscribers.collector');

/////////////////////////////////////////////////////////////////


Route::get('dashboard/reports', [
    'uses' => 'SuperAdminController@getReports',
    'as' => 'get.reports',
    'middleware' => ['role:Super Admin'],
]);

Route::get('custom-data-input', [
    'uses' => 'CustomDataInputController@getCustomDataInput',
    'as' => 'get.custom',
    'middleware' => ['role:Accountant'],
]);

Route::post('custom-data-input', [
    'uses' => 'CustomDataInputController@postCustomDataInput',
    'as' => 'post.custom',
    'middleware' => ['role:Accountant'],
]);

Route::get('export-user', function () {
    ini_set('memory_limit', '-1');
    return (new Rap2hpoutre\FastExcel\FastExcel(App\Subscriber::where('status', 'Active')->select(['name', 'last_name', 'specific_address', 'address'])->get()))->download('XXX.xlsx');
});

Route::get('export-billing', function () {
    ini_set('memory_limit', '-1');
    try {
        $active_subscribers = App\Subscriber::where('status', 'active')->get();

        $new = [];

        foreach($active_subscribers as $k => $v) {
                if(number_format($v->billings->sum('balance'),2) == '0.00') {
                    continue;
                }
                $new[$k]['id'] = $v->id;
                $new[$k]['name'] = $v->name.' '.$v->last_name;
                $new[$k]['address'] = $v->address;
                $new[$k]['contact_number'] = $v->contact_number;
                $new[$k]['balance'] = number_format($v->billings->sum('balance'),2);
                $new[$k]['sort'] = $v->billings->sum('balance');
        }

        usort($new, function($a, $b) {
            return $a['sort'] <=> $b['sort'];
        });

        $list = collect($new);

        return (new Rap2hpoutre\FastExcel\FastExcel($list))->download('balance.xlsx');
    } catch (Exception $e) {
        echo 'Message: ' .$e->getMessage();
    }

})->name('export.billing');


Route::get('export-fee', function () {
    ini_set('memory_limit', '-1');
    try {
        $active_subscribers = App\Subscriber::where('status', 'active')->get();

        $new = [];

        foreach($active_subscribers as $k => $v) {
                $new[$k]['id'] = $v->id;
                $new[$k]['name'] = $v->name.' '.$v->last_name;
                $new[$k]['address'] = $v->address;
                $new[$k]['contact_number'] = $v->contact_number;
                $new[$k]['balance'] = number_format($v->billings->sum('balance'),2);
                // $new[$k]['sort_balance'] = $v->billings->sum('balance');
                $new[$k]['fee'] = @$v->subscription->rate;
                $new[$k]['last_payment'] = @$v->payments()->latest()->first()->date_paid;
                $new[$k]['collector'] = @$v->collector;
        }

        // usort($new, function($a, $b) {
        //     return $a['sort_balance'] <=> $b['sort_balance'];
        // });

        $list = collect($new);

        // $list = $xlist->transform(function(array $item) {
        //     return array_except($item, ['sort_balance']);
        // });

        return (new Rap2hpoutre\FastExcel\FastExcel($list))->download('subscriber_fee.xlsx');
    } catch (Exception $e) {
        echo 'Message: ' .$e->getMessage();
    }

})->name('export.subscribers.fee');
