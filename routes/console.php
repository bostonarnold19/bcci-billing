
<?php

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
 */

use App\Role;
use App\User;

Artisan::command('user', function () {
    $role_accountant = Role::where('name', 'Accountant')->first();

    $princessangela = new User();
    $princessangela->name = 'Princess Angela Gelliaparang';
    $princessangela->email = 'princessangela@broadcable.com';
    $princessangela->password = bcrypt('princessangela');
    $princessangela->save();
    $princessangela->roles()->attach($role_accountant);

})->describe('Display an inspiring quote');

Artisan::command('balance', function () {
    $subscribers = App\Subscriber::all();
    foreach ($subscribers as $subscriber) {
        $subscriber->balance = $subscriber->billings->sum('balance') ?? 0;
        $subscriber->save();
        echo $subscriber->name . ' ' . $subscriber->last_name . "\n";
    }

})->describe('Display an inspiring quote');

Artisan::command('delete-billing', function () {
    // Delete Billing records that do not have any associated Subscribers
   $deletedCount = DB::table('billings')
                      ->leftJoin('billing_subscriber', 'billings.id', '=', 'billing_subscriber.billing_id')
                      ->whereNull('billing_subscriber.subscriber_id')
                      ->delete();

    // Echo the number of deleted records
    echo "Deleted {$deletedCount} orphaned billing records.\n";

})->describe('Deletes billing records with no associated subscribers');

Artisan::command('count-billing', function () {

    $billings = App\Billing::count();
    echo $billings . "\n";

})->describe('Display an inspiring quote');


Artisan::command('inspire', function () {

    //AI month auto generate
    // foreach ($array as $x) {
    // $subscribers = App\Subscriber::where('status', 'Active')->where('id', $x)->get();
    // }

    $subscribers = App\Subscriber::where('status', 'Active')->get();

    $start = Carbon\Carbon::now('Asia/Manila')->startOfMonth();
    $end = Carbon\Carbon::now('Asia/Manila')->endOfMonth();
    foreach ($subscribers as $subscriber) {

        if (!$subscriber->billings()->exists()) {
            continue;
        }

        $carbonFormatBilling = Carbon\Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);

        $carbonBetween = Carbon\Carbon::parse($carbonFormatBilling)->between($start, $end);

        if ($carbonFormatBilling->lte($end)) {

            if ($carbonBetween == false) {

                $carbonFormat = Carbon\Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
                $billing_period_end = Carbon\Carbon::parse($carbonFormat->addDays(29))->format('Y-m-d');
                $billing_date = Carbon\Carbon::parse($carbonFormat->addDays(1))->format('Y-m-d');
                $due_date = Carbon\Carbon::parse($carbonFormat->addDays(4))->format('Y-m-d');
                $newBilling = new App\Billing;
                $newBilling->amount_due = $subscriber->subscription->rate;
                $newBilling->status = 'Pending';
                $newBilling->balance = $subscriber->subscription->rate;
                $newBilling->billing_date = $billing_date;
                $newBilling->billing_period_start = $subscriber->billings->sortBy('billing_date')->last()->billing_date;
                $newBilling->billing_period_end = $billing_period_end;
                $newBilling->due_date = $due_date;
                $newBilling->save();
                $subscriber->billings()->attach($newBilling);
                $fee = new App\Fee;
                $fee->subscription_fee = $subscriber->subscription->rate;
                $newBilling->fee()->save($fee);
                echo $subscriber->name . ' ' . $subscriber->last_name . "\n";
            }
        }
    }
    echo "DONE";

    // DELETE BILLING
    // $billings = App\Billing::take(1410)->skip(1410 * 9)->get();
    // $count = 0;
    // foreach ($billings as $billing) {
    //     if (empty($billing->subscriber()->first())) {
    //         echo $billing->id . "\n";
    //         $billing->delete();
    //     }
    // }

    // CHECKING ARREARS FOR INACTIVE USER
    // $temp = 0;
    // foreach (App\Subscriber::where('status', 'inactive')->get() as $subscriber) {
    //     $balance = $subscriber->billings->sum('balance');
    //     $temp = $balance + $temp;
    // }
    // echo $total_arrears = $temp;

    // INITIAL DATA
    // $path = public_path('file.xlsx');
    // $collection = (new Rap2hpoutre\FastExcel\FastExcel)->import($path);

    // foreach ($collection as $collect) {
    //     $subscriber = new App\Subscriber;
    //     $subscriber->name = $collect['first_name'];
    //     $subscriber->last_name = $collect['last_name'];
    //     $subscriber->address = $collect['address'];
    //     $subscriber->status = 'Active';
    //     if (!empty($collect['date_of_installation'])) {
    //         $subscriber->date_of_installation = "2018-12-" . $collect['date_of_installation'];
    //     } else {
    //         $subscriber->date_of_installation = null;
    //     }
    //     $subscriber->save();

    //     $newDeposit = new App\Deposit;
    //     $newDeposit->amount = 0;
    //     $subscriber->deposit()->save($newDeposit);

    //     $subscription = new App\Subscription;
    //     $subscription->rate = (int) $collect['rate'];
    //     $subscriber->subscription()->save($subscription);

    //     echo $subscriber->name . ' ' . $subscriber->last_name . "\n";
    // }

})->describe('Display an inspiring quote');


Artisan::command('add-collector', function () {
    $subscribers = App\Subscriber::all();
    $count = 0;
    foreach ($subscribers as $subscriber) {
        $array = [];

        $payments = $subscriber->payments;

        if(count($payments) != 0) {
            $count++;
            foreach($payments as $k => $v) {
                if(isset($array[$v->collector])) {
                    $array[$v->collector]++;
                } else {
                    $array[$v->collector] = 1;
                }
            }
            $maxVal = max($array);
            $maxKey = array_search($maxVal, $array);

            $subscriber->collector = $maxKey;
            $subscriber->save();
            echo $subscriber->name . ' ' . $subscriber->last_name . ' - '. $maxKey . "\n";
        }
    }
    echo $count."\n";

})->describe('Display an inspiring quote');


Artisan::command('subscriber-no-collector:active', function () {
    $subscribers = App\Subscriber::where('collector', null)->where('status', 'Active')->get();
    $count = 0;
    foreach ($subscribers as $subscriber) {
        echo $subscriber->name . ' ' . $subscriber->last_name . "\n";
    }
    echo $count."\n";

})->describe('Display an inspiring quote');


Artisan::command('subscriber-no-collector:inactive', function () {
    $subscribers = App\Subscriber::where('collector', null)->where('status', 'Inactive')->get();
    $count = 0;
    foreach ($subscribers as $subscriber) {
        echo $subscriber->name . ' ' . $subscriber->last_name . "\n";
    }
    echo $count."\n";

})->describe('Display an inspiring quote');
