<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subscriber_id' => 'required|exists:subscribers,id',
            'official_receipt' => 'required|unique:payments',
            'collector' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/|exists:users,name',
            'date_paid' => 'required|date_format:M/d/Y',
            'amount_paid' => 'required|numeric|min:1',
            'billing_pending' => 'in:Pending',
            'billing_paid' => 'in:Paid',
            'billing_free' => 'in:Free',
        ];
    }
}
