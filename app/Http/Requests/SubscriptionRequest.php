<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubscriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:40',
            'address' => 'required|min:10',
            'date_of_installation' => 'required|date_format:M/d/Y',
            'contact_number' => 'numeric|unique:subscribers',
            'status' => 'required|in:Active',

            'subscriber_rate' => 'required|numeric',

            'billing_status' => 'required|in:Pending',

            'wire_length' => 'numeric',
            'installation_fee' => 'required|numeric',
            'wire_fee' => 'required|numeric',
        ];
    }
}
