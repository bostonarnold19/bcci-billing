<?php

namespace App\Http\Controllers;

use Activity;
use App\Billing;
use App\Fee;
use App\Http\Controllers\Controller;
use App\Subscriber;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;

class BillingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'amount_due' => 'required|numeric',
            'balance' => 'required|numeric|max:' . $request->amount_due,
        ]);
        try {
            DB::beginTransaction();

            $subscriber = Subscriber::find($request->subscriber_id);

            $carbonFormat = Carbon::createFromFormat('M/d/Y', $request->billing_date);
            $billing_date = Carbon::parse($carbonFormat)->format('Y-m-d');

            $carbonFormat = Carbon::createFromFormat('M/d/Y', $request->billing_date);
            $billing_period_end = Carbon::parse($carbonFormat->subDays(1))->format('Y-m-d');
            $due_date = Carbon::parse($carbonFormat->addDays(5))->format('Y-m-d');
            $billing_period_start = Carbon::parse($carbonFormat->subDays(34))->format('Y-m-d');

            $billing = new Billing;
            $billing->billing_date = $billing_date;
            $billing->billing_period_start = $billing_period_start;
            $billing->billing_period_end = $billing_period_end;
            $billing->due_date = $due_date;
            $billing->amount_due = $request->amount_due;
            $billing->balance = $request->balance;
            $billing->status = $request->balance == 0 ? 'Paid' : 'Pending';
            $billing->save();
            $subscriber->billings()->attach($billing);

            $fee = new Fee;
            $billing->fee()->save($fee);

            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Billing',
                'action' => 'POST',
                'state' => 'SUCCESS',
                'details' => 'Manually added a billing for subscriber ID :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name,
                'data' => json_encode($billing),
            ]);

            DB::commit();
            Session::flash('flash_message', 'Billing has been added.');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }
        return redirect()->back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'amount_due' => 'required|numeric',
            'balance' => 'required|numeric|max:' . $request->amount_due,
        ]);
        try {
            DB::beginTransaction();

            $carbonFormat = Carbon::createFromFormat('M/d/Y', $request->billing_date);
            $billing_date = Carbon::parse($carbonFormat)->format('Y-m-d');

            $carbonFormat = Carbon::createFromFormat('M/d/Y', $request->billing_date);
            $billing_period_end = Carbon::parse($carbonFormat->subDays(1))->format('Y-m-d');
            $due_date = Carbon::parse($carbonFormat->addDays(5))->format('Y-m-d');
            $billing_period_start = Carbon::parse($carbonFormat->subDays(34))->format('Y-m-d');

            $billing = Billing::find($id);
            $billing->billing_date = $billing_date;
            $billing->billing_period_start = $billing_period_start;
            $billing->billing_period_end = $billing_period_end;
            $billing->due_date = $due_date;

            $billing->amount_due = $request->amount_due;
            $billing->balance = $request->balance;
            $billing->status = $request->balance == 0 ? 'Paid' : 'Pending';
            $billing->update();

            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Billing',
                'action' => 'PATCH',
                'state' => 'SUCCESS',
                'details' => 'Update a billing for subscriber ID :' . $billing->subscriber()->first()->id . ', ' . $billing->subscriber()->first()->name . ' ' . $billing->subscriber()->first()->last_name,
                'data' => json_encode($billing),
            ]);

            DB::commit();
            Session::flash('flash_message', 'Billing has been updated.');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $billing = Billing::find($id);
            $billing->payments()->delete();
            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Billing',
                'action' => 'DELETE',
                'state' => 'SUCCESS',
                'details' => 'Delete a billing for subscriber ID :' . $billing->subscriber()->first()->id . ', ' . $billing->subscriber()->first()->name . ' ' . $billing->subscriber()->first()->last_name,
                'data' => json_encode($billing),
            ]);
            $billing->delete();
            DB::commit();
            Session::flash('flash_message', 'Billing has been deleted.');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }
        return redirect()->back();
    }
}
