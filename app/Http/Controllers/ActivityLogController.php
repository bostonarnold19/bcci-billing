<?php

namespace App\Http\Controllers;

use Activity;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ActivityLogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $val = $request->search;

        $activities = Activity::where(function ($query) use ($val) {

            $query->where('details', 'like', '%' . $val . '%');

        })->orderBy('id', 'desc')->paginate(50);

        return view('custom.activity', compact('activities'));

    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
