<?php

namespace App\Http\Controllers;

use Activity;
use App\Http\Controllers\Controller;
use App\Subscriber;
use App\Subscription;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Session;

class SubscriberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'address' => 'required|min:10',
            'rate' => 'required|numeric',
            'name' => 'required|max:30',
            'last_name' => 'required|max:30',
            'collector' => 'required|max:100',
            'contact_number' => 'numeric|unique:subscribers,contact_number,' . $id,
            'date_of_installation' => 'date_format:M/d/Y',
        ]);
        try {
            DB::beginTransaction();

            $carbonFormat = Carbon::createFromFormat('M/d/Y', $request->date_of_installation);
            $date_of_installation = Carbon::parse($carbonFormat)->format('Y-m-d');

            $subscriber = Subscriber::find($id);
            $subscriber->name = ucwords($request->name);
            $subscriber->last_name = ucwords($request->last_name);
            $subscriber->contact_number = $request->contact_number;
            $subscriber->address = ucwords($request->address);
            $subscriber->date_of_installation = $date_of_installation;
            $subscriber->collector = $request->collector;
            $subscriber->update();

            $subscription = Subscription::where('subscriber_id', $id)->first();

            if ($subscription->rate == $request->rate) {
                Activity::log([
                    'contentId' => auth()->user()->id,
                    'content' => 'Subscriber',
                    'action' => 'PATCH',
                    'state' => 'SUCCESS',
                    'details' => 'Update subscriber Info :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name,
                    'data' => json_encode($subscriber),
                ]);
            } else {
                Activity::log([
                    'contentId' => auth()->user()->id,
                    'content' => 'Subscriber',
                    'action' => 'PATCH',
                    'state' => 'SUCCESS',
                    'details' => 'Update subscriber subscription rate :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name . 'OLD Rate : ' . $subscription->rate . ', NEW Rate : ' . $request->rate,
                    'data' => json_encode($subscriber),
                ]);
            }

            $subscription->rate = $request->rate;
            $subscription->update();

            DB::commit();
            Session::flash('flash_message', 'Subscriber has been updated.');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $subscriber = Subscriber::find($id);

            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Subscriber',
                'action' => 'DELETE',
                'state' => 'SUCCESS',
                'details' => 'Delete subscriber ID :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name,
                'data' => json_encode($subscriber),
            ]);

            $subscriber->billings()->delete();
            $subscriber->payments()->delete();
            $subscriber->delete();
            DB::commit();
            Session::flash('flash_message', 'Subscriber has been deleted.');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }
        return redirect()->route('dashboard');
    }
}
