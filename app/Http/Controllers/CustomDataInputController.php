<?php

namespace App\Http\Controllers;

use App\Billing;
use App\Deposit;
use App\Fee;
use App\Http\Controllers\Controller;
use App\Subscriber;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Redirect;
use Session;

class CustomDataInputController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getCustomDataInput()
    {
        return view('custom.data_input');
    }

    public function postCustomDataInput(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:40',
            'last_name' => 'required|max:40',
            'address' => 'required|min:10',
            'contact_number' => 'numeric|unique:subscribers',
            'status' => 'required|in:Active',
            'subscriber_rate' => 'required|numeric',
            'billing_status' => 'required|in:Pending',
            'balance' => 'required|numeric',
        ]);

        $carbonFormat1 = Carbon::createFromFormat('M/d/Y', $request->billing_date);
        $billing_date = Carbon::parse($carbonFormat1)->format('Y-m-d');

        $subscriber = new Subscriber;
        $subscriber->last_name = ucwords($request->last_name);
        $subscriber->name = ucwords($request->name);
        $subscriber->address = ucwords($request->address);
        $subscriber->contact_number = $request->contact_number;
        $subscriber->status = $request->status;
        $subscriber->date_of_installation = '';
        $subscriber->save();

        $newDeposit = new Deposit;
        $newDeposit->amount = 0;
        $subscriber->deposit()->save($newDeposit);

        $subscription = new Subscription;
        $subscription->rate = $request->subscriber_rate;
        $subscriber->subscription()->save($subscription);

        $billing = new Billing;
        $billing->billing_date = $billing_date;
        $billing->billing_period_start = $billing_date;
        $billing->billing_period_end = $billing_date;
        $billing->due_date = $billing_date;
        $billing->amount_due = $request->balance;
        $billing->balance = $request->balance;
        $billing->status = $request->billing_status;
        $billing->save();
        $subscriber->billings()->attach($billing);

        $fee = new Fee;
        $billing->fee()->save($fee);

        Session::flash('flash_message', 'You added an old subscriber. (Current/Active Subscriber)');
        return Redirect::route('get.custom');
    }

}
