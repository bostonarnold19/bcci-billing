<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Subscriber;
use Illuminate\Http\Request;
use Session;

class StatementOfAccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {
        $subscriber = Subscriber::find($id);

        $billings = $subscriber->billings()->where('status', 'Pending')->orderBy('created_at', 'ASC')->get();

        if ($billings->first()) {
            return view('custom.statement', compact('subscriber', 'billings'));
        } else {
            Session::flash('flash_message', 'This user has no debt. Nothing to print');
            return redirect()->back();
        }

    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
