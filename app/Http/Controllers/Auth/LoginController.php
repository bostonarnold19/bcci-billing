<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\LoginRequest;
use Auth;
use App\Role;
use App\Fee;
use App\Subscriber;
use App\Billing;
use Redirect;
use Carbon\Carbon;
use App\User;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function getLogin(Request $request)
    {
        // $this->authGenerate();

        return view('auth.login');
    }

    public function postLogin(LoginRequest $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            if(Auth::user()->hasRole('Collector') && Auth::user()->hasRole('Accountant')) {
                return Redirect::route('dashboard');
            } elseif(Auth::user()->hasRole('Collector')) {
                return Redirect::route('collector.subscribers');
            } else {
                return Redirect::route('dashboard');
            }
        }

        return redirect()->back();
    }

    public function getLogout()
    {
        Auth::logout();

        // $this->authGenerate();

        return view('auth.login');
    }

    public function authGenerate()
    {
        $subscribers = Subscriber::where('status', 'Active')->get();

        $start = Carbon::now('Asia/Manila')->startOfMonth();
        $end = Carbon::now('Asia/Manila')->endOfMonth();

        //AI month auto generate
        foreach ($subscribers as $subscriber)
        {
            $carbonFormatBilling = Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
            $carbonBetween = Carbon::parse($carbonFormatBilling)->between($start, $end);

            if ($carbonFormatBilling->lte($end)) {

                if ($carbonBetween == false) {

                    $carbonFormat = Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
                    $billing_period_end = Carbon::parse($carbonFormat->addDays(29))->format('Y-m-d');
                    $billing_date = Carbon::parse($carbonFormat->addDays(1))->format('Y-m-d');
                    $due_date = Carbon::parse($carbonFormat->addDays(4))->format('Y-m-d');

                    $newBilling = new Billing;
                    $newBilling->amount_due = $subscriber->subscription->rate;
                    $newBilling->status = 'Pending';
                    $newBilling->balance = $subscriber->subscription->rate;
                    $newBilling->billing_period_start = $subscriber->billings->sortBy('billing_date')->last()->billing_date;
                    $newBilling->billing_period_end = $billing_period_end;
                    $newBilling->billing_date = $billing_date;
                    $newBilling->due_date = $due_date;
                    $newBilling->save();
                    $subscriber->billings()->attach($newBilling);

                    $fee = new Fee;
                    $fee->subscription_fee = $subscriber->subscription->rate;
                    $newBilling->fee()->save($fee);
                }

            }

        }
    }

}



