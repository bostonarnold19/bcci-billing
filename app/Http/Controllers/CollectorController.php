<?php

namespace App\Http\Controllers;

use Activity;
use App\Billing;
use App\Deposit;
use App\Fee;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentRequest;
use App\Http\Requests\ReconnectionRequest;
use App\Http\Requests\SubscriptionRequest;
use App\Payment;
use App\Role;
use App\Subscriber;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Image;
use Redirect;
use Session;

class CollectorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSubscriber(Request $request)
    {
        $user = auth()->user();
        $count = Subscriber::where('collector', $user->name)->where('status', 'Active')->count();

        $collectors = Role::where('name', 'Collector')->firstorfail();

        $val = $request->search;

        $subscribers = Subscriber::where('collector', $user->name)->where(function ($query) use ($val) {

            $query->whereRaw('CONCAT(name, " ", last_name) LIKE ? ', '%' . $val . '%')
                ->orwhere('address', 'like', '%' . $val . '%');

        })->where('status', 'Active')->paginate(50);

        return view('collector.subscribers', compact('subscribers', 'count', 'collectors'));
    }

    public function pay(PaymentRequest $request)
    {
        try {
            DB::beginTransaction();
            $subscriber = Subscriber::find($request->subscriber_id);
            $pendingBillings = $subscriber->billings->where('status', 'Pending');

            $this->validate($request, [
                'discount' => 'numeric',
            ]);

            if (empty($request->discount)) {

                $request->discount = 0;

            }

            $discount = $request->discount;

            if ($subscriber->status == 'Inactive') {

                if ($subscriber->billings->sum('balance') < $request->amount_paid) {

                    return Redirect::route('billings.subscriber', $subscriber->id)->withErrors('Invalid transaction');

                }
            }

            $carbonFormat = Carbon::createFromFormat('M/d/Y', $request->date_paid);
            $date_paid = Carbon::parse($carbonFormat)->format('Y-m-d');

            $payment = new Payment;
            $payment->official_receipt = $request->official_receipt;
            $payment->collector = $request->collector;
            $payment->date_paid = $date_paid;
            $payment->amount_paid = $request->amount_paid;
            $payment->save();
            $subscriber->payments()->attach($payment->id);

            $amount_paid = $request->amount_paid + $discount;

            //EXISTING BILLING UPDATE
            foreach ($pendingBillings as $billing) {
                if ($amount_paid >= $billing->balance) {

                    $updateBilling = Billing::find($billing->id);
                    $updateBilling->balance = 0;
                    $updateBilling->status = $request->billing_paid;
                    $updateBilling->update();
                    $payment->billings()->attach($billing);

                    $amount_paid = $amount_paid - $billing->balance;

                    $fee = Fee::where('billing_id', $updateBilling->id)->first();
                    $fee->discount = $discount;
                    $fee->update();

                    $discount = 0;

                } elseif ($amount_paid < $billing->balance) {

                    $amount_paid = $billing->balance - $amount_paid;

                    $updateBilling = Billing::find($billing->id);
                    $updateBilling->balance = $amount_paid;
                    $updateBilling->update();

                    $fee = Fee::where('billing_id', $updateBilling->id)->first();
                    $fee->discount = $discount;
                    $fee->update();

                    $discount = 0;

                    if ($amount_paid != $billing->balance) {
                        $payment->billings()->attach($billing);
                    }

                    $amount_paid = 0;
                    break;
                }

            }

            //ADVANCE PAYMENT ONE MONTH ADVANCE LESS THAN AMOUNT DUE
            if ($amount_paid != 0) {

                $advancePayment = $amount_paid / $subscriber->subscription->rate;

                if ((int) $advancePayment == 0) {

                    $carbonFormat = Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
                    $billing_period_end = Carbon::parse($carbonFormat->addDays(29))->format('Y-m-d');
                    $billing_date = Carbon::parse($carbonFormat->addDays(1))->format('Y-m-d');
                    $due_date = Carbon::parse($carbonFormat->addDays(4))->format('Y-m-d');

                    $newBilling = new Billing;
                    $newBilling->amount_due = $subscriber->subscription->rate;
                    $newBilling->status = $request->billing_pending;
                    $newBilling->balance = $subscriber->subscription->rate - $amount_paid;
                    $newBilling->billing_period_start = $subscriber->billings->sortBy('billing_date')->last()->billing_date;
                    $newBilling->billing_period_end = $billing_period_end;
                    $newBilling->billing_date = $billing_date;
                    $newBilling->due_date = $due_date;
                    $newBilling->save();
                    $subscriber->billings()->attach($newBilling);
                    $payment->billings()->attach($newBilling);

                    $fee = new Fee;
                    $fee->subscription_fee = $subscriber->subscription->rate;
                    $newBilling->fee()->save($fee);
                } elseif ((int) $advancePayment >= 10) {
                    //One year + one month free advance payment invoke billing/generate billing

                    $monthFree = $advancePayment / 10;
                    $advancePayment = $advancePayment + (int) $monthFree;

                    for ($i = 0; $i < $advancePayment; $i++) {
                        $subscriber = Subscriber::find($request->subscriber_id);

                        $carbonFormat = Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
                        $billing_period_end = Carbon::parse($carbonFormat->addDays(29))->format('Y-m-d');
                        $billing_date = Carbon::parse($carbonFormat->addDays(1))->format('Y-m-d');
                        $due_date = Carbon::parse($carbonFormat->addDays(4))->format('Y-m-d');

                        $newBilling = new Billing;
                        $newBilling->amount_due = $subscriber->subscription->rate;
                        $newBilling->status = $request->billing_pending;
                        $newBilling->balance = $subscriber->subscription->rate;
                        $newBilling->billing_period_start = $subscriber->billings->sortBy('billing_date')->last()->billing_date;
                        $newBilling->billing_period_end = $billing_period_end;
                        $newBilling->billing_date = $billing_date;
                        $newBilling->due_date = $due_date;
                        $newBilling->save();
                        $subscriber->billings()->attach($newBilling);

                        $fee = new Fee;
                        $fee->subscription_fee = $subscriber->subscription->rate;
                        $newBilling->fee()->save($fee);
                    }

                    $subscriber = Subscriber::find($request->subscriber_id);
                    $pendingBillings = $subscriber->billings->where('status', 'Pending');

                    //Paying bills
                    foreach ($pendingBillings as $billing) {
                        if ($amount_paid >= $billing->balance) {

                            $updateBilling = Billing::find($billing->id);
                            $updateBilling->balance = 0;
                            $updateBilling->status = $request->billing_paid;
                            $updateBilling->update();
                            $payment->billings()->attach($billing);

                            $amount_paid = $amount_paid - $billing->balance;

                            $fee = Fee::where('billing_id', $updateBilling->id)->first();
                            $fee->discount = $discount;
                            $fee->update();

                            $discount = 0;

                        } elseif ($amount_paid < $billing->balance) {

                            $amount_paid = $billing->balance - $amount_paid;

                            $updateBilling = Billing::find($billing->id);
                            $updateBilling->balance = $amount_paid;
                            $updateBilling->update();

                            $fee = Fee::where('billing_id', $updateBilling->id)->first();
                            $fee->discount = $discount;
                            $fee->update();

                            $discount = 0;

                            if ($amount_paid != $billing->balance) {
                                $payment->billings()->attach($billing);
                            }

                            break;
                        }

                    }

                    //Free months payment
                    for ($i = 0; $i < (int) $monthFree; $i++) {
                        $subscriber = Subscriber::find($request->subscriber_id);
                        $pendingBilling = $subscriber->billings->where('status', 'Pending')->first();

                        $updateBilling = Billing::find($pendingBilling->id);
                        $updateBilling->balance = 0;
                        $updateBilling->status = $request->billing_free;
                        $updateBilling->update();
                    }

                    $extra = $amount_paid % $subscriber->subscription->rate;

                    if ($extra != 0) {

                        $subscriber = Subscriber::find($request->subscriber_id);
                        $pendingBilling = $subscriber->billings->where('status', 'Pending')->first();

                        $updateBilling = Billing::find($pendingBilling->id);
                        $updateBilling->balance = $amount_paid;
                        $updateBilling->status = $request->billing_pending;
                        $updateBilling->update();
                    }

                } else {
                    //ADVANCE MORE THAN ONE MONTH 3 to 10months

                    for ($i = 0; $i < $advancePayment; $i++) {
                        $subscriber = Subscriber::find($request->subscriber_id);

                        $carbonFormat = Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
                        $billing_period_end = Carbon::parse($carbonFormat->addDays(29))->format('Y-m-d');
                        $billing_date = Carbon::parse($carbonFormat->addDays(1))->format('Y-m-d');
                        $due_date = Carbon::parse($carbonFormat->addDays(4))->format('Y-m-d');

                        $newBilling = new Billing;
                        $newBilling->amount_due = $subscriber->subscription->rate;
                        $newBilling->status = $request->billing_pending;
                        $newBilling->balance = $subscriber->subscription->rate;
                        $newBilling->billing_period_start = $subscriber->billings->sortBy('billing_date')->last()->billing_date;
                        $newBilling->billing_period_end = $billing_period_end;
                        $newBilling->billing_date = $billing_date;
                        $newBilling->due_date = $due_date;
                        $newBilling->save();
                        $subscriber->billings()->attach($newBilling);

                        $fee = new Fee;
                        $fee->subscription_fee = $subscriber->subscription->rate;
                        $newBilling->fee()->save($fee);
                    }

                    $subscriber = Subscriber::find($request->subscriber_id);
                    $pendingBillings = $subscriber->billings->where('status', 'Pending');

                    foreach ($pendingBillings as $billing) {
                        if ($amount_paid >= $billing->balance) {

                            $updateBilling = Billing::find($billing->id);
                            $updateBilling->balance = 0;
                            $updateBilling->status = $request->billing_paid;
                            $updateBilling->update();
                            $payment->billings()->attach($billing);

                            $fee = Fee::where('billing_id', $updateBilling->id)->first();
                            $fee->discount = $discount;
                            $fee->update();

                            $discount = 0;

                            $amount_paid = $amount_paid - $billing->balance;
                        } elseif ($amount_paid < $billing->balance) {

                            $amount_paid = $billing->balance - $amount_paid;

                            $updateBilling = Billing::find($billing->id);
                            $updateBilling->balance = $amount_paid;
                            $updateBilling->update();

                            $fee = Fee::where('billing_id', $updateBilling->id)->first();
                            $fee->discount = $discount;
                            $fee->update();

                            $discount = 0;

                            if ($amount_paid != $billing->balance) {
                                $payment->billings()->attach($billing);
                            }

                            break;
                        }

                    }
                }

            }

            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Subscriber',
                'action' => 'PATCH',
                'state' => 'SUCCESS',
                'details' => 'Payment Subscriber :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name,
                'data' => json_encode($subscriber),
            ]);

            DB::commit();
            Session::flash('flash_message', 'You paid the balance of ' . $subscriber->name);
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }
        return Redirect::route('collector.ty', $subscriber->id);
    }

    public function ty($id)
    {
        $user = auth()->user();

        $subscriber = Subscriber::where('collector', $user->name)->where('id',$id)->first();

        return view('collector.ty', compact('subscriber'));
    }

    public function performance(Request $request)
    {
        $user = auth()->user();
        $collections = Payment::where('collector', $user->name);

        if ($request->date != null) {

            $carbonDate = Carbon::createFromFormat('M/d/Y', $request->date);
            $report = Carbon::parse($carbonDate)->format('Y-m-d');

            $collections = $collections->where('date_paid', $report)
                ->get();

        } else {

            $carbonFormatNow = Carbon::now('Asia/Manila');
            $report = Carbon::parse($carbonFormatNow)->format('Y-m-d');

            $collections = $collections->where('date_paid', $report)
                 ->get();

        }

        return view('collector.performance', compact('collections', 'report'));
    }
}
