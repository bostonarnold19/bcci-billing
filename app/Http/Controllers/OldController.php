   public function patchSubscriberContactNumber(Request $request)
    {
        $this->validate($request, [
            'subscriber_contact_number' => 'numeric|unique:subscribers,contact_number,' . $request->id,
        ]);

        if ($request->ajax()) {

            $subscriber = Subscriber::find($request->id);
            $subscriber->contact_number = $request->subscriber_contact_number;
            $subscriber->update();

            return response()->json([
                'contact_number' => $subscriber->contact_number,
                'success' => 'Subscriber contact number was successfully been updated',
            ]);
        }

    }

    public function patchSubscriberDateOfInstallation(Request $request)
    {
        $this->validate($request, [
            'subscriber_date_of_installation' => 'date_format:M/d/Y',
        ]);

        if ($request->ajax()) {

            $carbonFormat = Carbon::createFromFormat('M/d/Y', $request->subscriber_date_of_installation);
            $date_of_installation = Carbon::parse($carbonFormat)->format('Y-m-d');

            $subscriber = Subscriber::find($request->id);
            $subscriber->date_of_installation = $date_of_installation;
            $subscriber->update();

            return response()->json([
                'date_of_installation' => Carbon::parse($subscriber->date_of_installation)->format('M/d/Y'),
                'success' => 'Subscriber installation date was successfully been updated',
            ]);
        }

    }

    public function patchSubscriberName(Request $request)
    {
        $this->validate($request, [
            'subscriber_name' => 'required|max:30',
        ]);

        if ($request->ajax()) {

            $subscriber = Subscriber::find($request->id);
            $subscriber->name = ucwords($request->subscriber_name);
            $subscriber->update();

            return response()->json([
                'name' => $subscriber->name,
                'success' => 'Subscriber name was successfully been updated',
            ]);
        }

    }

    public function patchSubscriberRate(Request $request)
    {
        $this->validate($request, [
            'subscriber_rate' => 'required|numeric',
        ]);

        if ($request->ajax()) {

            $subscription = Subscription::find($request->id);
            $subscription->rate = $request->subscriber_rate;
            $subscription->update();

            return response()->json([
                'rate' => $subscription->rate,
                'success' => 'Subscriber rate was successfully been updated',
            ]);
        }

    }

    public function patchSubscriberAddress(Request $request)
    {
        $this->validate($request, [
            'subscriber_address' => 'required|min:10',
        ]);

        if ($request->ajax()) {

            $subscriber = Subscriber::find($request->id);
            $subscriber->address = ucwords($request->subscriber_address);
            $subscriber->update();

            return response()->json([
                'address' => $subscriber->address,
                'success' => 'Subscriber address was successfully been updated',
            ]);
        }

    }
