<?php

namespace App\Http\Controllers;

use Activity;
use App\Billing;
use App\Deposit;
use App\Fee;
use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentRequest;
use App\Http\Requests\ReconnectionRequest;
use App\Http\Requests\SubscriptionRequest;
use App\Payment;
use App\Role;
use App\Subscriber;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Image;
use Redirect;
use Session;

class SuperAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSubscriberStatementOfAccount($id)
    {
        $subscriber = Subscriber::find($id);
        $billings = $subscriber->billings()->orderBy('id', 'desc')->get();

        $collectors = Role::where('name', 'Collector')->firstorfail();

        return view('admin.statement_of_account', compact('collectors', 'subscriber', 'billings'));
    }

    public function postDeposit(Request $request)
    {
        try {
            DB::beginTransaction();
            $subscriber = Subscriber::find($request->id);

            $pendingBillings = $subscriber->billings->where('status', 'Pending');

            $deposit = $subscriber->deposit->amount;

            //EXISTING BILLING UPDATE
            foreach ($pendingBillings as $billing) {
                if ($deposit >= $billing->balance) {

                    $updateBilling = Billing::find($billing->id);
                    $updateBilling->balance = 0;
                    $updateBilling->status = 'Paid by deposit';
                    $updateBilling->update();

                    $deposit = $deposit - $billing->balance;
                } elseif ($deposit < $billing->balance) {

                    $deposit = $billing->balance - $deposit;

                    $updateBilling = Billing::find($billing->id);
                    $updateBilling->balance = $deposit;
                    $updateBilling->update();

                    $deposit = 0;
                    break;
                }

            }

            if ($deposit != 0) {

                $advancePayment = $deposit / $subscriber->subscription->rate;

                if ((int) $advancePayment == 0) {

                    $carbonFormat = Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
                    $billing_period_end = Carbon::parse($carbonFormat->addDays(29))->format('Y-m-d');
                    $billing_date = Carbon::parse($carbonFormat->addDays(1))->format('Y-m-d');
                    $due_date = Carbon::parse($carbonFormat->addDays(4))->format('Y-m-d');

                    $newBilling = new Billing;
                    $newBilling->amount_due = $subscriber->subscription->rate;
                    $newBilling->status = 'Pending';
                    $newBilling->balance = $subscriber->subscription->rate - $deposit;
                    $newBilling->billing_period_start = $subscriber->billings->sortBy('billing_date')->last()->billing_date;
                    $newBilling->billing_period_end = $billing_period_end;
                    $newBilling->billing_date = $billing_date;
                    $newBilling->due_date = $due_date;
                    $newBilling->save();
                    $subscriber->billings()->attach($newBilling);

                    $fee = new Fee;
                    $fee->subscription_fee = $subscriber->subscription->rate;
                    $newBilling->fee()->save($fee);

                } else {
                    //ADVANCE MORE THAN ONE MONTH 3 to 10months

                    for ($i = 0; $i < $advancePayment; $i++) {
                        $subscriber = Subscriber::find($request->id);

                        $carbonFormat = Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
                        $billing_period_end = Carbon::parse($carbonFormat->addDays(29))->format('Y-m-d');
                        $billing_date = Carbon::parse($carbonFormat->addDays(1))->format('Y-m-d');
                        $due_date = Carbon::parse($carbonFormat->addDays(4))->format('Y-m-d');

                        $newBilling = new Billing;
                        $newBilling->amount_due = $subscriber->subscription->rate;
                        $newBilling->status = 'Pending';
                        $newBilling->balance = $subscriber->subscription->rate;
                        $newBilling->billing_period_start = $subscriber->billings->sortBy('billing_date')->last()->billing_date;
                        $newBilling->billing_period_end = $billing_period_end;
                        $newBilling->billing_date = $billing_date;
                        $newBilling->due_date = $due_date;
                        $newBilling->save();
                        $subscriber->billings()->attach($newBilling);

                        $fee = new Fee;
                        $fee->subscription_fee = $subscriber->subscription->rate;
                        $newBilling->fee()->save($fee);
                    }

                    $subscriber = Subscriber::find($request->id);
                    $pendingBillings = $subscriber->billings->where('status', 'Pending');

                    foreach ($pendingBillings as $billing) {
                        if ($deposit >= $billing->balance) {

                            $updateBilling = Billing::find($billing->id);
                            $updateBilling->balance = 0;
                            $updateBilling->status = 'Paid by deposit';
                            $updateBilling->update();

                            $deposit = $deposit - $billing->balance;
                        } elseif ($deposit < $billing->balance) {

                            $deposit = $billing->balance - $deposit;

                            $updateBilling = Billing::find($billing->id);
                            $updateBilling->balance = $deposit;
                            $updateBilling->update();

                            $deposit = 0;
                            break;
                        }
                    }

                }

            }

            $findDeposit = Deposit::find($subscriber->deposit->id);
            $findDeposit->amount = 0;
            $findDeposit->update();

            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Subscriber',
                'action' => 'POST',
                'state' => 'SUCCESS',
                'details' => 'Use Subscriber Deposit for Apartment Type :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name,
                'data' => json_encode($subscriber),
            ]);

            DB::commit();
            Session::flash('flash_message', 'Deposit has been successfully used');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }

        return Redirect::route('billings.subscriber', $request->id);
    }

    public function patchSubscriberBilling(Request $request)
    {
        $this->validate($request, [
            'amount_due' => 'required|numeric',
            'balance' => 'required|numeric',
        ]);
        try {
            DB::beginTransaction();
            $billing = Billing::find($request->billing_id);
            $billing->amount_due = $request->amount_due;
            $billing->balance = $request->balance;
            $billing->update();
            DB::commit();
            Session::flash('flash_message', 'Billing has been updated.');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }

        return Redirect::route('billings.subscriber', $request->subscriber_id);
    }

    public function getAddOldSubscriber()
    {
        return view('admin.add_old_subscriber');
    }

    public function postAddOldSubscriber(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:40',
            'last_name' => 'required|max:40',
            'address' => 'required|min:10',
            'contact_number' => 'numeric|unique:subscribers',
            'status' => 'required|in:Inactive',
            'subscriber_rate' => 'required|numeric',
            'billing_status' => 'required|in:Pending',
            'balance' => 'required|numeric',
        ]);
        try {
            DB::beginTransaction();
            $carbonFormatNow = Carbon::now('Asia/Manila');
            $billing_date = Carbon::parse($carbonFormatNow)->format('Y-m-d');

            $subscriber = new Subscriber;
            $subscriber->name = ucwords($request->name);
            $subscriber->last_name = ucwords($request->last_name);
            $subscriber->address = ucwords($request->address);
            $subscriber->contact_number = $request->contact_number;
            $subscriber->status = $request->status;
            $subscriber->date_of_installation = $billing_date;
            $subscriber->save();

            $newDeposit = new Deposit;
            $newDeposit->amount = 0;
            $subscriber->deposit()->save($newDeposit);

            $subscription = new Subscription;
            $subscription->rate = $request->subscriber_rate;
            $subscriber->subscription()->save($subscription);

            $billing = new Billing;
            $billing->billing_date = $billing_date;
            $billing->billing_period_start = $billing_date;
            $billing->billing_period_end = $billing_date;
            $billing->due_date = $billing_date;
            $billing->amount_due = $request->balance;
            $billing->balance = $request->balance;
            $billing->status = $request->billing_status;
            $billing->save();
            $subscriber->billings()->attach($billing);

            $fee = new Fee;
            $billing->fee()->save($fee);

            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Subscriber',
                'action' => 'POST',
                'state' => 'SUCCESS',
                'details' => 'Add an OLD subscriber ID :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name,
                'data' => json_encode($subscriber),
            ]);

            DB::commit();
            Session::flash('flash_message', 'You added an old subscriber.');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }

        return Redirect::route('billings.subscriber', $subscriber->id);
    }

    public function getCollectors()
    {
        $collectors = Role::where('name', 'Collector')->firstorfail();

        return view('admin.collectors', compact('collectors'));
    }

    public function postCollector(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:30|regex:/(^[A-Za-z0-9 ]+$)+/',
        ]);
        try {
            DB::beginTransaction();
            $collectorExist = User::where('name', ucwords($request->name))->get()->first();

            if ($collectorExist) {

                $collectorExist->roles()->detach(Role::where('name', 'Collector')->first());
                $collectorExist->roles()->attach(Role::where('name', 'Collector')->first());

            } else {

                $collector = new User;
                $collector->name = ucwords($request->name);
                $collector->save();
                $collector->roles()->attach(Role::where('name', 'Collector')->first());
            }

            DB::commit();
            Session::flash('flash_message', 'Collector added!');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }

        return Redirect::route('get.collectors');
    }

    public function getAllSubscribersReportDaily(Request $request)
    {
        if ($request->date != null) {

            $carbonDate = Carbon::createFromFormat('M/d/Y', $request->date);
            $report = Carbon::parse($carbonDate)->format('Y-m-d');


            $collections = Payment::where('date_paid', $report)
                ->get();

        } else {

            $carbonFormatNow = Carbon::now('Asia/Manila');
            $report = Carbon::parse($carbonFormatNow)->format('Y-m-d');

            $collections = Payment::where('date_paid', $report)
                 ->get();

        }

        return view('admin.collector_all_daily_report_subscriber', compact('collections', 'report'));
    }

    public function getSubscribersReportDaily(Request $request)
    {
        $collector = $request->collector;
        if ($request->date != null) {

            $carbonDate = Carbon::createFromFormat('M/d/Y', $request->date);
            $report = Carbon::parse($carbonDate)->format('Y-m-d');


            $collections = Payment::where('date_paid', $report)
                ->with('billings')
                ->where('collector', $request->collector)
                ->get();

        } else {

            $carbonFormatNow = Carbon::now('Asia/Manila');
            $report = Carbon::parse($carbonFormatNow)->format('Y-m-d');

            $collections = Payment::where('date_paid', $report)
                ->with('billings')
                ->where('collector', $request->collector)
                 ->get();

        }

        return view('admin.collector_daily_report_subscriber', compact('collections', 'report', 'collector'));
    }

    public function getCollectorDailyReport(Request $request)
    {
        if ($request->date != null) {

            $carbonDate = Carbon::createFromFormat('M/d/Y', $request->date);
            $report = Carbon::parse($carbonDate)->format('Y-m-d');
            $org_date = $request->date;

            $collectors = Payment::where('date_paid', $report)
                ->select('collector', \DB::raw('sum(payments.amount_paid) amount_paid'), \DB::raw('count(payments.id) count'))
                ->groupBy('collector')
                ->get();

        } else {

            $carbonFormatNow = Carbon::now('Asia/Manila');
            $report = Carbon::parse($carbonFormatNow)->format('Y-m-d');

            $carbonFormatNowOrg = Carbon::now('Asia/Manila');
            $org_date = Carbon::parse($carbonFormatNow)->format('M/d/Y');

            $collectors = Payment::where('date_paid', $report)
                ->select('collector', \DB::raw('sum(payments.amount_paid) amount_paid'), \DB::raw('count(payments.id) count'))
                ->groupBy('collector')
                ->get();

        }

        return view('admin.collector_daily_report', compact('collectors', 'report', 'org_date'));
    }

    public function getSubscribersReportMonthly(Request $request)
    {
        $collector = $request->collector;
        if ($request->date != null) {

            $date = $request->date . '/1';

            $start = Carbon::createFromFormat('M/Y/d', $date);
            $end = Carbon::createFromFormat('M/Y/d', $date)->endOfMonth();

            $collections = Payment::where('date_paid', '>=', $start)
                ->where('date_paid', '<=', $end)
                ->where('collector', $request->collector)
                ->with('billings')
                ->get();

        } else {

            $start = Carbon::now('Asia/Manila')->startOfMonth();
            $end = Carbon::now('Asia/Manila')->endOfMonth();
            // $collectors = Payment::all()->groupBy('collector');

            $collections = Payment::where('date_paid', '>=', $start)
                ->where('date_paid', '<=', $end)
                ->where('collector', $request->collector)
                ->with('billings')
                ->get();
        }

        return view('admin.collector_monthly_report_subscriber', compact('collections', 'start', 'end', 'collector'));
    }


    public function getCollectorMonthlyReport(Request $request)
    {
        if ($request->date != null) {

            $date = $request->date . '/1';

            $start = Carbon::createFromFormat('M/Y/d', $date);
            $end = Carbon::createFromFormat('M/Y/d', $date)->endOfMonth();
            $org_date = $request->date;


            // $collectors = Payment::all()->groupBy('collector');

            $collectors = Payment::where('date_paid', '>=', $start)
                ->where('date_paid', '<=', $end)
                ->select('collector', \DB::raw('sum(payments.amount_paid) amount_paid'),\DB::raw('count(payments.id) count'))
                ->groupBy('collector')
                ->get();

        } else {

            $start = Carbon::now('Asia/Manila')->startOfMonth();
            $end = Carbon::now('Asia/Manila')->endOfMonth();
            // $collectors = Payment::all()->groupBy('collector');
            $org_date = Carbon::parse($start)->format('M/Y');

            $collectors = Payment::where('date_paid', '>=', $start)
                ->where('date_paid', '<=', $end)
                ->select('collector', \DB::raw('sum(payments.amount_paid) amount_paid'),\DB::raw('count(payments.id) count'))
                ->groupBy('collector')
                ->get();
        }

        return view('admin.collector_monthly_report', compact('collectors', 'start', 'end', 'org_date'));
    }

    public function patchCollectorRemove(Request $request)
    {
        try {
            DB::beginTransaction();
            $collector = User::where('id', $request->collector_id)->firstorfail();
            $collector->roles()->detach(Role::where('name', 'Collector')->first());
            DB::commit();
            Session::flash('flash_message', $collector->name . ' has been removed');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }
        return Redirect::route('get.collectors');
    }

    public function getReports()
    {
        $active_subscribers = Subscriber::where('status', 'active')->paginate(30, ['*'], 'active');

        $inactive_subscribers = Subscriber::where('status', 'inactive')->paginate(30, ['*'], 'inactive');

        //hard input number is inactive users
        // $total_arrears = Billing::sum('balance') - 1259044;
        $total_arrears = Billing::sum('balance');

        return view('admin.reports', compact('active_subscribers', 'inactive_subscribers', 'total_arrears'));
    }

    public function postSubscriberTransfer(Request $request)
    {
        $this->validate($request, [
            'wire_fee' => 'required|numeric',
            'transfer_fee' => 'required|numeric',
            'wire_length' => 'numeric',
            'billing_date' => 'required|date_format:M/d/Y',
            'billing_status' => 'required|in:Pending',
        ]);
        try {
            DB::beginTransaction();
            if (empty($request->wire_length)) {

                $request->wire_length = 0;
            }

            $subscriber = Subscriber::find($request->id);

            $carbonFormat = Carbon::createFromFormat('M/d/Y', $request->billing_date);
            $billing_date = Carbon::parse($carbonFormat)->format('Y-m-d');

            $total_wire_fee = $request->wire_length * $request->wire_fee;
            $total_amount_due = $request->transfer_fee + $total_wire_fee;

            $billing = new Billing;
            $billing->billing_date = $billing_date;
            $billing->billing_period_start = $billing_date;
            $billing->billing_period_end = $billing_date;
            $billing->due_date = $billing_date;
            $billing->amount_due = $total_amount_due;
            $billing->balance = $total_amount_due;
            $billing->status = $request->billing_status;
            $billing->save();
            $subscriber->billings()->attach($billing);

            $fee = new Fee;
            $fee->transfer_fee = $request->transfer_fee;
            $fee->wire_length = $request->wire_length;
            $fee->wire_fee = $total_wire_fee;
            $billing->fee()->save($fee);

            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Subscriber',
                'action' => 'PATCH',
                'state' => 'SUCCESS',
                'details' => 'Subscriber Transfer :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name,
                'data' => json_encode($subscriber),
            ]);

            DB::commit();
            Session::flash('flash_message', $subscriber->name . ' has been transfered!');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }

        return Redirect::route('profile.subscriber', $subscriber->id);
    }

    public function patchSubscriberPicture(Request $request)
    {
        $subscriber = Subscriber::find($request->subscriber_id);

        if ($request->hasFile('picture')) {

            $this->validate($request, [
                'picture' => 'required|mimes:jpeg,jpg,png|max:25600',
            ]);

            $picture = $request->file('picture');
            $filename = time() . '.' . $picture->getClientOriginalExtension();

            $background = Image::canvas(1000, 1000);

            $image = Image::make($picture)->resize(1000, 1000, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            });

            $background->insert($image, 'center');
            $background->save(public_path('img/' . $filename));

            $subscriber->picture = $filename;
            $subscriber->save();

            Session::flash('flash_message', 'Image successfully updated!');
        } else {

            $subscriber->picture = 'default.jpg';
            $subscriber->save();

            Session::flash('flash_message', 'Image successfully removed!');
        }

        return Redirect::route('profile.subscriber', $subscriber->id);
    }

    public function getFees(Request $request)
    {
        if ($request->ajax()) {

            $billing = Billing::find($request->id);

            return response()->json([
                'installation_fee' => $billing->fee->installation_fee,
                'transfer_fee' => $billing->fee->transfer_fee,
                'reconnection_fee' => $billing->fee->reconnection_fee,
                'subscription_fee' => $billing->fee->subscription_fee,
                'wire_length' => $billing->fee->wire_length,
                'wire_fee' => $billing->fee->wire_fee,
                'disconnection_fee' => $billing->fee->disconnection_fee,
                'deposit_fee' => $billing->fee->deposit_fee,
                'discount' => $billing->fee->discount,
            ]);
        }

    }

    public function patchReconnectSubscriber(ReconnectionRequest $request)
    {
        try {
            DB::beginTransaction();
            $subscriber = Subscriber::find($request->subscriber_id);
            $subscriber->status = 'Active';
            $subscriber->update();

            $carbonFormat = Carbon::createFromFormat('M/d/Y', $request->reconnection_date);
            $reconnection_date = Carbon::parse($carbonFormat)->format('Y-m-d');

            $billing = new Billing;
            $billing->billing_date = $reconnection_date;
            $billing->billing_period_start = $reconnection_date;
            $billing->billing_period_end = $reconnection_date;
            $billing->due_date = $reconnection_date;
            $billing->amount_due = $request->amount_due;
            $billing->balance = $request->amount_due;
            $billing->status = $request->status;
            $billing->save();
            $subscriber->billings()->attach($billing);

            $fee = new Fee;
            $fee->reconnection_fee = $request->amount_due;
            $billing->fee()->save($fee);

            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Subscriber',
                'action' => 'PATCH',
                'state' => 'SUCCESS',
                'details' => 'Reconnect Subscriber :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name,
                'data' => json_encode($subscriber),
            ]);

            DB::commit();
            Session::flash('flash_message', $subscriber->name . ' has been reconnected');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }

        return Redirect::route('billings.subscriber', $subscriber->id);

    }

    public function patchDisconnectSubscriber(Request $request)
    {

        try {
            DB::beginTransaction();
            if (empty($request->amount_due)) {

                $request->amount_due = 0;

            }

            $subscriber = Subscriber::find($request->subscriber_id);
            $subscriber->status = 'Inactive';
            $subscriber->update();

            $carbonFormatNow = Carbon::now('Asia/Manila');
            $now = Carbon::parse($carbonFormatNow)->format('Y-m-d');

            $carbonFormat = Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
            $billing_period_end = Carbon::parse($carbonFormat->addDays(29))->format('Y-m-d');
            $billing_date = Carbon::parse($carbonFormat->addDays(1))->format('Y-m-d');
            $due_date = Carbon::parse($carbonFormat->addDays(4))->format('Y-m-d');

            $newBilling = new Billing;
            $newBilling->amount_due = $request->amount_due;
            $newBilling->status = 'Pending';
            $newBilling->balance = $request->amount_due;
            $newBilling->billing_period_start = $subscriber->billings->sortBy('billing_date')->last()->billing_date;
            $newBilling->billing_period_end = $now;
            $newBilling->billing_date = $now;
            $newBilling->due_date = $now;
            $newBilling->save();
            $subscriber->billings()->attach($newBilling);

            $fee = new Fee;
            $fee->disconnection_fee = $request->amount_due;
            $newBilling->fee()->save($fee);

            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Subscriber',
                'action' => 'PATCH',
                'state' => 'SUCCESS',
                'details' => 'Disconnect Subscriber :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name . ', Note : ' . @$request->note,
                'data' => json_encode($subscriber),
            ]);

            DB::commit();
            Session::flash('flash_message', $subscriber->name . ' has been disconnected');
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }

        return Redirect::route('billings.subscriber', $subscriber->id);
    }

    public function getDashboard(Request $request)
    {
        $count = Subscriber::where('status', 'Active')->count();

        $collectors = Role::where('name', 'Collector')->firstorfail();

        $val = $request->search;

        $subscribers = Subscriber::where(function ($query) use ($val) {

            $query->whereRaw('CONCAT(name, " ", last_name) LIKE ? ', '%' . $val . '%')
                ->orwhere('address', 'like', '%' . $val . '%');

        })->where('status', 'Active')->orderBy('id')->paginate(50);

        return view('admin.subscribers', compact('subscribers', 'count', 'collectors'));
    }

    public function getInactiveSubscribers(Request $request)
    {
        $count = Subscriber::where('status', 'Inactive')->count();

        $collectors = Role::where('name', 'Collector')->firstorfail();

        $val = $request->search;

        $subscribers = Subscriber::where(function ($query) use ($val) {

            $query->whereRaw('CONCAT(name, " ", last_name) LIKE ? ', '%' . $val . '%')
                ->orwhere('address', 'like', '%' . $val . '%');

        })->where('status', 'Inactive')->paginate(50);

        return view('admin.inactive_subscribers', compact('subscribers', 'count', 'collectors'));
    }

    public function postPay(PaymentRequest $request)
    {
        try {
            DB::beginTransaction();
            $subscriber = Subscriber::find($request->subscriber_id);
            $pendingBillings = $subscriber->billings->where('status', 'Pending');

            $this->validate($request, [
                'discount' => 'numeric',
            ]);

            if (empty($request->discount)) {

                $request->discount = 0;

            }

            $discount = $request->discount;

            if ($subscriber->status == 'Inactive') {

                if ($subscriber->billings->sum('balance') < $request->amount_paid) {

                    return Redirect::route('billings.subscriber', $subscriber->id)->withErrors('Invalid transaction');

                }
            }

            $carbonFormat = Carbon::createFromFormat('M/d/Y', $request->date_paid);
            $date_paid = Carbon::parse($carbonFormat)->format('Y-m-d');

            $payment = new Payment;
            $payment->official_receipt = $request->official_receipt;
            $payment->collector = $request->collector;
            $payment->date_paid = $date_paid;
            $payment->amount_paid = $request->amount_paid;
            $payment->save();
            $subscriber->payments()->attach($payment->id);

            $amount_paid = $request->amount_paid + $discount;

            //EXISTING BILLING UPDATE
            foreach ($pendingBillings as $billing) {
                if ($amount_paid >= $billing->balance) {

                    $updateBilling = Billing::find($billing->id);
                    $updateBilling->balance = 0;
                    $updateBilling->status = $request->billing_paid;
                    $updateBilling->update();
                    $payment->billings()->attach($billing);

                    $amount_paid = $amount_paid - $billing->balance;

                    $fee = Fee::where('billing_id', $updateBilling->id)->first();
                    $fee->discount = $discount;
                    $fee->update();

                    $discount = 0;

                } elseif ($amount_paid < $billing->balance) {

                    $amount_paid = $billing->balance - $amount_paid;

                    $updateBilling = Billing::find($billing->id);
                    $updateBilling->balance = $amount_paid;
                    $updateBilling->update();

                    $fee = Fee::where('billing_id', $updateBilling->id)->first();
                    $fee->discount = $discount;
                    $fee->update();

                    $discount = 0;

                    if ($amount_paid != $billing->balance) {
                        $payment->billings()->attach($billing);
                    }

                    $amount_paid = 0;
                    break;
                }

            }

            //ADVANCE PAYMENT ONE MONTH ADVANCE LESS THAN AMOUNT DUE
            if ($amount_paid != 0) {

                $advancePayment = $amount_paid / $subscriber->subscription->rate;

                if ((int) $advancePayment == 0) {

                    $carbonFormat = Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
                    $billing_period_end = Carbon::parse($carbonFormat->addDays(29))->format('Y-m-d');
                    $billing_date = Carbon::parse($carbonFormat->addDays(1))->format('Y-m-d');
                    $due_date = Carbon::parse($carbonFormat->addDays(4))->format('Y-m-d');

                    $newBilling = new Billing;
                    $newBilling->amount_due = $subscriber->subscription->rate;
                    $newBilling->status = $request->billing_pending;
                    $newBilling->balance = $subscriber->subscription->rate - $amount_paid;
                    $newBilling->billing_period_start = $subscriber->billings->sortBy('billing_date')->last()->billing_date;
                    $newBilling->billing_period_end = $billing_period_end;
                    $newBilling->billing_date = $billing_date;
                    $newBilling->due_date = $due_date;
                    $newBilling->save();
                    $subscriber->billings()->attach($newBilling);
                    $payment->billings()->attach($newBilling);

                    $fee = new Fee;
                    $fee->subscription_fee = $subscriber->subscription->rate;
                    $newBilling->fee()->save($fee);
                } elseif ((int) $advancePayment >= 10) {
                    //One year + one month free advance payment invoke billing/generate billing

                    $monthFree = $advancePayment / 10;
                    $advancePayment = $advancePayment + (int) $monthFree;

                    for ($i = 0; $i < $advancePayment; $i++) {
                        $subscriber = Subscriber::find($request->subscriber_id);

                        $carbonFormat = Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
                        $billing_period_end = Carbon::parse($carbonFormat->addDays(29))->format('Y-m-d');
                        $billing_date = Carbon::parse($carbonFormat->addDays(1))->format('Y-m-d');
                        $due_date = Carbon::parse($carbonFormat->addDays(4))->format('Y-m-d');

                        $newBilling = new Billing;
                        $newBilling->amount_due = $subscriber->subscription->rate;
                        $newBilling->status = $request->billing_pending;
                        $newBilling->balance = $subscriber->subscription->rate;
                        $newBilling->billing_period_start = $subscriber->billings->sortBy('billing_date')->last()->billing_date;
                        $newBilling->billing_period_end = $billing_period_end;
                        $newBilling->billing_date = $billing_date;
                        $newBilling->due_date = $due_date;
                        $newBilling->save();
                        $subscriber->billings()->attach($newBilling);

                        $fee = new Fee;
                        $fee->subscription_fee = $subscriber->subscription->rate;
                        $newBilling->fee()->save($fee);
                    }

                    $subscriber = Subscriber::find($request->subscriber_id);
                    $pendingBillings = $subscriber->billings->where('status', 'Pending');

                    //Paying bills
                    foreach ($pendingBillings as $billing) {
                        if ($amount_paid >= $billing->balance) {

                            $updateBilling = Billing::find($billing->id);
                            $updateBilling->balance = 0;
                            $updateBilling->status = $request->billing_paid;
                            $updateBilling->update();
                            $payment->billings()->attach($billing);

                            $amount_paid = $amount_paid - $billing->balance;

                            $fee = Fee::where('billing_id', $updateBilling->id)->first();
                            $fee->discount = $discount;
                            $fee->update();

                            $discount = 0;

                        } elseif ($amount_paid < $billing->balance) {

                            $amount_paid = $billing->balance - $amount_paid;

                            $updateBilling = Billing::find($billing->id);
                            $updateBilling->balance = $amount_paid;
                            $updateBilling->update();

                            $fee = Fee::where('billing_id', $updateBilling->id)->first();
                            $fee->discount = $discount;
                            $fee->update();

                            $discount = 0;

                            if ($amount_paid != $billing->balance) {
                                $payment->billings()->attach($billing);
                            }

                            break;
                        }

                    }

                    //Free months payment
                    for ($i = 0; $i < (int) $monthFree; $i++) {
                        $subscriber = Subscriber::find($request->subscriber_id);
                        $pendingBilling = $subscriber->billings->where('status', 'Pending')->first();

                        $updateBilling = Billing::find($pendingBilling->id);
                        $updateBilling->balance = 0;
                        $updateBilling->status = $request->billing_free;
                        $updateBilling->update();
                    }

                    $extra = $amount_paid % $subscriber->subscription->rate;

                    if ($extra != 0) {

                        $subscriber = Subscriber::find($request->subscriber_id);
                        $pendingBilling = $subscriber->billings->where('status', 'Pending')->first();

                        $updateBilling = Billing::find($pendingBilling->id);
                        $updateBilling->balance = $amount_paid;
                        $updateBilling->status = $request->billing_pending;
                        $updateBilling->update();
                    }

                } else {
                    //ADVANCE MORE THAN ONE MONTH 3 to 10months

                    for ($i = 0; $i < $advancePayment; $i++) {
                        $subscriber = Subscriber::find($request->subscriber_id);

                        $carbonFormat = Carbon::createFromFormat('Y-m-d', $subscriber->billings->sortBy('billing_date')->last()->billing_date);
                        $billing_period_end = Carbon::parse($carbonFormat->addDays(29))->format('Y-m-d');
                        $billing_date = Carbon::parse($carbonFormat->addDays(1))->format('Y-m-d');
                        $due_date = Carbon::parse($carbonFormat->addDays(4))->format('Y-m-d');

                        $newBilling = new Billing;
                        $newBilling->amount_due = $subscriber->subscription->rate;
                        $newBilling->status = $request->billing_pending;
                        $newBilling->balance = $subscriber->subscription->rate;
                        $newBilling->billing_period_start = $subscriber->billings->sortBy('billing_date')->last()->billing_date;
                        $newBilling->billing_period_end = $billing_period_end;
                        $newBilling->billing_date = $billing_date;
                        $newBilling->due_date = $due_date;
                        $newBilling->save();
                        $subscriber->billings()->attach($newBilling);

                        $fee = new Fee;
                        $fee->subscription_fee = $subscriber->subscription->rate;
                        $newBilling->fee()->save($fee);
                    }

                    $subscriber = Subscriber::find($request->subscriber_id);
                    $pendingBillings = $subscriber->billings->where('status', 'Pending');

                    foreach ($pendingBillings as $billing) {
                        if ($amount_paid >= $billing->balance) {

                            $updateBilling = Billing::find($billing->id);
                            $updateBilling->balance = 0;
                            $updateBilling->status = $request->billing_paid;
                            $updateBilling->update();
                            $payment->billings()->attach($billing);

                            $fee = Fee::where('billing_id', $updateBilling->id)->first();
                            $fee->discount = $discount;
                            $fee->update();

                            $discount = 0;

                            $amount_paid = $amount_paid - $billing->balance;
                        } elseif ($amount_paid < $billing->balance) {

                            $amount_paid = $billing->balance - $amount_paid;

                            $updateBilling = Billing::find($billing->id);
                            $updateBilling->balance = $amount_paid;
                            $updateBilling->update();

                            $fee = Fee::where('billing_id', $updateBilling->id)->first();
                            $fee->discount = $discount;
                            $fee->update();

                            $discount = 0;

                            if ($amount_paid != $billing->balance) {
                                $payment->billings()->attach($billing);
                            }

                            break;
                        }

                    }
                }

            }

            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Subscriber',
                'action' => 'PATCH',
                'state' => 'SUCCESS',
                'details' => 'Payment Subscriber :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name,
                'data' => json_encode($subscriber),
            ]);

            DB::commit();
            Session::flash('flash_message', 'You paid the balance of ' . $subscriber->name);
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }
        return Redirect::route('billings.subscriber', $subscriber->id);

    }

    public function getAddSubscriber()
    {
        return view('admin.add_subscriber');
    }

    public function getStoreSubscriber(SubscriptionRequest $request)
    {
        try {
            DB::beginTransaction();

            $carbonFormat = Carbon::createFromFormat('M/d/Y', $request->date_of_installation);
            $date_of_installation = Carbon::parse($carbonFormat)->format('Y-m-d');

            if (empty($request->wire_length)) {

                $request->wire_length = 0;
            }

            $total_wire_fee = $request->wire_length * $request->wire_fee;
            $total_amount_due = $request->installation_fee + $total_wire_fee;

            $subscriber = new Subscriber;
            $subscriber->name = ucwords($request->name);
            $subscriber->last_name = ucwords($request->last_name);
            $subscriber->address = ucwords($request->address);
            $subscriber->contact_number = $request->contact_number;
            $subscriber->status = $request->status;
            $subscriber->date_of_installation = $date_of_installation;
            $subscriber->save();

            $subscription = new Subscription;
            $subscription->rate = $request->subscriber_rate;
            $subscriber->subscription()->save($subscription);

            if ($request->deposit == "apartment") {

                $deposit = $subscription->rate * 2;

            } else {

                $deposit = 0;

            }

            $newDeposit = new Deposit;
            $newDeposit->amount = $deposit;
            $subscriber->deposit()->save($newDeposit);

            $billing = new Billing;
            $billing->billing_date = $date_of_installation;
            $billing->billing_period_start = $date_of_installation;
            $billing->billing_period_end = $date_of_installation;
            $billing->due_date = $date_of_installation;
            $billing->amount_due = $total_amount_due + $deposit;
            $billing->balance = $total_amount_due + $deposit;
            $billing->status = $request->billing_status;
            $billing->save();
            $subscriber->billings()->attach($billing);

            $fee = new Fee;
            $fee->installation_fee = $request->installation_fee;
            $fee->wire_length = $request->wire_length;
            $fee->wire_fee = $total_wire_fee;
            $fee->deposit_fee = $deposit;
            $billing->fee()->save($fee);

            Activity::log([
                'contentId' => auth()->user()->id,
                'content' => 'Subscriber',
                'action' => 'POST',
                'state' => 'SUCCESS',
                'details' => 'New Subscriber :' . $subscriber->id . ', ' . $subscriber->name . ' ' . $subscriber->last_name,
                'data' => json_encode($subscriber),
            ]);

            Session::flash('flash_message', 'You added a new subscriber.');
            DB::commit();
        } catch (\Exception $e) {
            $status = 'error';
            $message = 'Internal Server Error. Try again later.';
            DB::rollBack();
        }
        return Redirect::route('billings.subscriber', $subscriber->id);
    }

    public function getSubscriberBillings($id)
    {
        $subscriber = Subscriber::find($id);
        $billings = $subscriber->billings()->orderBy('id', 'desc')->get();

        $firstbilling = $subscriber->billings()->first();

        $collectors = Role::where('name', 'Collector')->firstorfail();

        return view('admin.billings_subscriber', compact('collectors', 'subscriber', 'billings', 'firstbilling'));
    }

    public function getSubscriberPayments($id)
    {
        $subscriber = Subscriber::find($id);
        $payments = $subscriber->payments()->orderBy('id', 'desc')->get();

        return view('admin.payments_subscriber', compact('subscriber', 'payments'));
    }

    public function getSubscriberProfile($id)
    {
        $subscriber = Subscriber::find($id);

        $collectors = Role::where('name', 'Collector')->firstorfail();

        return view('admin.profile_subscriber', compact('subscriber','collectors'));
    }

}
