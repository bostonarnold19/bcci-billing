<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    public function fee()
    {
    	return $this->hasOne('App\Fee');
    }

    public function payments()
    {
    	return $this->belongsToMany('App\Payment');
    }

    public function subscriber()
    {
    	return $this->belongsToMany('App\Subscriber');
    }
}
