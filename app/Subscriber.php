<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    public function billings()
    {
        return $this->belongsToMany('App\Billing');
    }

    public function payments()
    {
        return $this->belongsToMany('App\Payment');
    }

    public function subscription()
    {
        return $this->hasOne('App\Subscription');
    }

    public function deposit()
    {
        return $this->hasOne('App\Deposit');
    }
}
