<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function billings()
    {
    	return $this->belongsToMany('App\Billing');
    }

    public function subscribers()
    {
        return $this->belongsToMany('App\Subscriber');
    }

    public function subscriber()
    {
        return $this->subscribers()->first();
    }

}
