<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'Super Admin')->first();
        $role_accountant = Role::where('name', 'Accountant')->first();
        $role_collector = Role::where('name', 'Collector')->first();

        $a1 = new User();
        $a1->name = 'Maria Editha Bautista';
        $a1->email = 'admin@broadcable.com';
        $a1->password = bcrypt('admin');
        $a1->save();
        $a1->roles()->attach($role_admin);

        $a2 = new User();
        $a2->name = 'Jennette Gonzales';
        $a2->email = 'jennette@broadcable.com';
        $a2->password = bcrypt('jennette');
        $a2->save();
        $a2->roles()->attach($role_accountant);

        $a3 = new User();
        $a3->name = 'Pau Pau';
        $a3->email = 'paupau@broadcable.com';
        $a3->password = bcrypt('paupau');
        $a3->save();
        $a3->roles()->attach($role_accountant);

        $a4 = new User();
        $a4->name = 'Jonah Mae';
        $a4->email = 'jonahmae08@broadcable.com';
        $a4->password = bcrypt('jonahmae08');
        $a4->save();
        $a4->roles()->attach($role_accountant);

        $a5 = new User();
        $a5->name = 'Arayat';
        $a5->email = 'arayat@broadcable.com';
        $a5->password = bcrypt('arayat');
        $a5->save();
        $a5->roles()->attach($role_accountant);

        $a6 = new User();
        $a6->name = 'Emmar Santos';
        $a6->save();
        $a6->roles()->attach($role_collector);

        $a7 = new User();
        $a7->name = 'Rainier David';
        $a7->save();
        $a7->roles()->attach($role_collector);

        $a8 = new User();
        $a8->name = 'Dennis Munoz';
        $a8->save();
        $a8->roles()->attach($role_collector);

        $a9 = new User();
        $a9->name = 'Rolly Aniciete';
        $a9->save();
        $a9->roles()->attach($role_collector);

        $a10 = new User();
        $a10->name = 'Arayat Office';
        $a10->save();
        $a10->roles()->attach($role_collector);

        $a11 = new User();
        $a11->name = 'Magalang Office';
        $a11->save();
        $a11->roles()->attach($role_collector);

        $a12 = new User();
        $a12->name = 'C001';
        $a12->save();
        $a12->roles()->attach($role_collector);

        $a13 = new User();
        $a13->name = 'C002';
        $a13->save();
        $a13->roles()->attach($role_collector);

        $a14 = new User();
        $a14->name = 'C003';
        $a14->save();
        $a14->roles()->attach($role_collector);

        $a15 = new User();
        $a15->name = 'Jervin Cunanan';
        $a15->email = 'jervin@broadcable.com';
        $a15->password = bcrypt('jervin');
        $a15->save();
        $a15->roles()->attach($role_accountant);

        $a16 = new User();
        $a16->name = 'Lady Munoz';
        $a16->email = 'lady@broadcable.com';
        $a16->password = bcrypt('lady');
        $a16->save();
        $a16->roles()->attach($role_accountant);
    }
}
