<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Admin = new Role();
        $Admin->name = 'Super Admin';
        $Admin->save();

        $Accountant = new Role();
        $Accountant->name = 'Accountant';
        $Accountant->save();

        $Collector = new Role();
        $Collector->name = 'Collector';
        $Collector->save();

        $Client = new Role();
        $Client->name = 'Client';
        $Client->save();
    }
}
