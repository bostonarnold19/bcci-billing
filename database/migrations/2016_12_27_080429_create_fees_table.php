<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('billing_id')->unsigned();
            $table->foreign('billing_id')->references('id')->on('billings')
                ->onDelete('cascade');
            $table->float('installation_fee', 9, 2)->nullable();
            $table->float('reconnection_fee', 9, 2)->nullable();
            $table->float('disconnection_fee', 9, 2)->nullable();
            $table->float('deposit_fee', 9, 2)->nullable();
            $table->float('discount', 9, 2)->nullable();
            $table->float('transfer_fee', 9, 2)->nullable();
            $table->float('wire_fee', 9, 2)->nullable();
            $table->float('subscription_fee', 9, 2)->nullable();
            $table->integer('wire_length')->nullable();
            $table->timestamps();
            $table->engine = 'InnoDB';

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fees');
    }
}
