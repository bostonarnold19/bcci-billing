<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentSubscriberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_subscriber', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('subscriber_id')->unsigned();
            $table->integer('payment_id')->unsigned();

            $table->foreign('subscriber_id')->references('id')->on('subscribers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('payment_id')->references('id')->on('payments')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->engine = 'InnoDB';

            $table->primary(['subscriber_id', 'payment_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_subscriber');
    }
}
