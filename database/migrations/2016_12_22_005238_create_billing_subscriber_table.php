<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingSubscriberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_subscriber', function (Blueprint $table) {
            $table->integer('subscriber_id')->unsigned();
            $table->integer('billing_id')->unsigned();

            $table->foreign('subscriber_id')->references('id')->on('subscribers')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('billing_id')->references('id')->on('billings')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->engine = 'InnoDB';

            $table->primary(['subscriber_id', 'billing_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billing_subscriber');
    }
}
