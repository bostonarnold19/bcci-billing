<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddedFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscribers', function ($table) {
            $table->string('last_name')->nullable()->after('name');
            $table->string('delete_comment')->nullable();
            $table->boolean('is_deleted')->default(0);
            $table->string('specific_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscribers', function ($table) {
            $table->dropColumn('last_name');
            $table->dropColumn('delete_comment');
            $table->dropColumn('is_deleted');
            $table->dropColumn('specific_address');
        });
    }
}
